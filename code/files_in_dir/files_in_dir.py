import os

data_dir = "./Data"
onlyfiles = [os.path.join(data_dir, f) for f in os.listdir(data_dir)
             if os.path.isfile(os.path.join(data_dir, f))]
onlyfiles = sorted(onlyfiles)
img_products = [x.split('/')[-1].split('.')[-1] for x in onlyfiles]
for ii in range(len(onlyfiles)):
    if "txt" in img_products[ii]:
        txt_filepath = onlyfiles[ii]
        print("txt file:", txt_filepath)
    if "xtt" in img_products[ii]:
        xtt_filepath = onlyfiles[ii]
        print("xtt file:", xtt_filepath)        

from dict_traverse import dict_traverse

testDict = {
    "A":{ 1:{"alpha":444} },
    "B":{ 2:{"beta":555} },
    "C":{ 3:{"gamma":666} },
    "D":{ 4:{"delta":777} },
}

value = dict_traverse(testDict, "gamma")
print("value:", value)

## Python Generators  
*What are generators in Python? Why are they useful? How is one used?*  


Generator functions resemble regular functions, but they yield generator objects. Let's see an example which generates Fibonacci numbers.  

```python
# A simple generator for Fibonacci Numbers
def fib(limit):
      
    # Initialize first two Fibonacci Numbers 
    a, b = 0, 1
  
    # One by one yield next Fibonacci Number
    while a < limit:
        yield a
        a, b = b, a + b
  
# Create a generator object
x = fib(5)
```

```python
# let's generate some values
print(x.__next__())
print(x.__next__())
print(x.__next__())
print(x.__next__())
print(x.__next__())
print(x.__next__())
```

output  
```
0
1
1
2
3
Traceback (most recent call last):
  File "main.py", line 21, in <module>
    print(x.__next__())
StopIteration
```

Oops, I went too far. Only 5 values are allowed for this generator, but I tried to generate 6. To generated all values without going past the bounds, just iterate over the object:  

```python
for val in x:
    print(val)
```

output  
```
0
1
1
2
3
```

A generator expression is a convenient way to succinctly describe certain common types of generators.  

```python
gen = (x for x in range(1,100) if x%11==0)

for val in gen:
    print(val)
```

output  
```
11
22
33
44
55
66
77
88
99
```    


#### Lazy iterators  

We can loop over generator objects like a lists, however, unlike lists, generators are "lazy iterators", meaning that they do not store their contents in memory and are evaluated on demand. If you find yourself about to create a large list which could consume significant memory, ask yourself whether a generator might be appropriate. If you need to, say reverse, or filter the list, then you might need it pre-computed, but if you want to generate value one-at-a-time, then a generator might be what you need. 

There is one thing to keep in mind, though. If the list is smaller than the running machine’s available memory, then list comprehensions can be faster to evaluate than the equivalent generator expression.   


#### Example use-cases of generators  

1. Reading large files   

```python
csv_gen = (row for row in open(file_name))

```

2. Infinite sequences  


#### How to compare memory usage: list vs. generator

```
>>> import sys
>>> nums_list = [i ** 2 for i in range(10000)]
>>> sys.getsizeof(nums_list)
85176
>>> nums_generator = (i ** 2 for i in range(10000))
>>> sys.getsizeof(nums_generator)
112
```

 



import csv, sqlite3

# create the database
conn = sqlite3.connect('./data/starforce_personnel.db')
cur = conn.cursor()


# create the "pilots" table populate fields from corresponding csv file
cur.execute('''CREATE TABLE pilots
             (forceId integer, name text, score real, shipmateId integer)''')

with open('./data/pilots.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    FIRSTROW = 1
    for row in readCSV:
        if not FIRSTROW: # don't read the header row
            if len(row)>0:
                cur.execute("INSERT INTO pilots VALUES (?,?,?,?)", row)
        else:
            FIRSTROW = 0


# create the "engineers" table populate fields from corresponding csv file
cur.execute('''CREATE TABLE engineers
             (forceId integer, name text, score real, shipmateId integer)''')

with open('./data/engineers.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    FIRSTROW = 1
    for row in readCSV:
        if not FIRSTROW: # don't read the header row
            if len(row)>0:
                cur.execute("INSERT INTO engineers VALUES (?,?,?,?)", row)
        else:
            FIRSTROW = 0


# commit (save) the changes and close the connection
conn.commit()
conn.close()




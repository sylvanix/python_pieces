import numpy as np


'''
Returns a list of neighboring point values, out to max_sigma distance from (0,0).
These values will be added to a (y,x) point from a map.
'''
def getAdjacency(max_sigma):
    span = [x for x in range(-(max_sigma), (max_sigma)+1)]
    adjacency = [(i,j) for i in span for j in span if (i,j) != (0,0)]
    return adjacency


'''
Keep only non-zero elements of map having at least nbNeighborsRequired neighbors at distance=1.
'''
def removeUnconnected(mapImg, nbNeighborsRequired):
    rows, cols = mapImg.shape
    idx = np.where(mapImg == 1) # (array([2, 2, 3, 6]), array([2, 3, 2, 7]))
    adjacency = getAdjacency(1) # 
    for ii in range(len(idx[0])):
        #print( idx[0][ii], idx[1][ii] )
        neighbors = ([(x[0]+idx[0][ii], x[1]+idx[1][ii]) for x in adjacency])
        # remove any cells that would lie outside the array
        combined = [x for x in neighbors if (0<x[0]<rows) and (0<x[1]<cols)]
        # are any of these connected indexes included in mapImg?
        mapXYs = [(idx[0][ii],idx[1][ii]) for ii in range(len(idx[0]))]
        candidates = [x for x in mapXYs if x in combined]
        if len(candidates) < nbNeighborsRequired:
            mapImg[idx[0][ii], idx[1][ii]] = 0

    return mapImg


'''
Replace valued for known "bad-pixels" with the mean value of their non-bad neighbors
'''
def getReplacementValue(ngidx, ii, Ximg, adjacency_distance):
    rows, cols = Ximg.shape
    adjacency = getAdjacency(adjacency_distance)
    neighbors = ([(x[0]+ngidx[ii][0], x[1]+ngidx[ii][1]) for x in adjacency])
    # remove any cells that would lie outside the array
    combined = [x for x in neighbors if (0<x[0]<rows) and (0<x[1]<cols)]
    # remove any cells also in ngidx
    combined = [x for x in combined if (x[0], x[1]) not in ngidx]
    replVal = np.mean(np.array([Ximg[x[0],x[1]] for x in combined]))
    return replVal
    

'''
Replace bad pixels in Ximg which are marked with value=1 in the BadPixMap
'''
def getReplacementValues(Ximg, BadPixMap, mask, adjacency_distance):
    goodMeanVal = np.mean(Ximg[Ximg*mask>0])
    #print("goodMeanVal: ", goodMeanVal)
    Replacements = np.zeros_like(BadPixMap)
    rows, cols = BadPixMap.shape
    ngidx = np.where(BadPixMap == 1)
    bads = [ (ngidx[0][jj],ngidx[1][jj]) for jj in range(len(ngidx[0])) ]
    for ii in range(len(ngidx[0])):
        #print('(y,x): ' , "(", ngidx[0][ii], ",", ngidx[1][ii], ")", sep='')
        for d in [x for x in range(1, adjacency_distance+1)]:
            #print("d:", d)
            adjacency = getAdjacency(d)
            neighbors = ([(x[0]+ngidx[0][ii], x[1]+ngidx[1][ii]) for x in adjacency])
            # remove any cells that would lie outside the array
            combined = [x for x in neighbors if (0<x[0]<rows) and (0<x[1]<cols)]
            # remove any cells also in ngidx
            combined = [x for x in combined if (x[0], x[1]) not in bads]
            #print(combined)
            
            # if there are no candidates after the 2nd pass (16 neighbors), then
            # set the replacement value to the image mean.
            if combined == [] and d==2:
                #print ("REPLACEMENT VALUE NOT FOUND.")
                replVal = goodMeanVal
                
            # if candidates are found, then set the replacement value to the mean
            # of the candidates
            else:
                replVal = np.mean(np.array([Ximg[x[0],x[1]] for x in combined]))
            #print("replVal:", replVal)
            
            # if candidates are found in the first pass (8 neighbors), then break
            # so a second pass is not performed
            if combined:
                break
            
        # replace this pixels value
        Ximg[ngidx[0][ii], ngidx[1][ii]] = replVal
        
    return Ximg
    
    
'''
Find indices of replacement pixels if any exist
'''
def getReplacementIndexes(BadPixMap, adjacency_distance):
    Replacements = np.zeros_like(BadPixMap)
    rows, cols = BadPixMap.shape
    ngidx = np.where(BadPixMap == 1)
    bads = [ (ngidx[0][jj],ngidx[1][jj]) for jj in range(len(ngidx[0])) ]
    candidates = []
    for ii in range(len(ngidx[0])):
        #print('(y,x): ' , "(", ngidx[0][ii], ",", ngidx[1][ii], ")", sep='')
        d = 1
        while (d<=2):
        #for d in [x for x in range(1, adjacency_distance+1)]:
            #print("d:", d)
            adjacency = getAdjacency(d)
            neighbors = ([(x[0]+ngidx[0][ii], x[1]+ngidx[1][ii]) for x in adjacency])
            #print("neighbors: ", bads[ii], neighbors)
            # # remove any cells that would lie outside the array
            combined = [x for x in neighbors if (0<x[0]<rows) and (0<x[1]<cols)]
            #print("border checking: ", bads[ii], combined)
            # remove any cells also in ngidx
            combined = [x for x in combined if (x[0], x[1]) not in bads]
            #print("exclude bads: ", bads[ii], combined)
            #print("")
            
            if (d==1) and len(combined)>0:
                candidates.append(combined)
                d += 2
            if (d==1) and len(combined)==0:
                d += 1
            if (d==2):
                candidates.append(combined)
                d += 1
        
    return (bads, candidates)
            
    
    
'''
Replace the pixel values
'''        
def replaceBadPixels(Ximg, bads, candidates, mask):
    goodMeanVal = np.mean(Ximg[(Ximg*mask)>0])
    #goodMeanVal = np.mean(Ximg)
    #print("goodMeanVal:", goodMeanVal)
    for ii in range(len(bads)):
        #print(bads[ii], candidates[ii])
        if candidates[ii]:
            values = [Ximg[x[0],x[1]] for x in candidates[ii]]
            replVal = np.mean(np.array(values))
        else:
            replVal = goodMeanVal
            
        # replace this pixels value
        Ximg[bads[ii][0], bads[ii][1]] = replVal
            
    return Ximg
            
            
    
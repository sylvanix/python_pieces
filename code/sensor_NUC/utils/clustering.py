import numpy as np
from sklearn.cluster import DBSCAN


# find clusters using DBSCAN; ignore clusters of size larger than max_pixel_size
# input image must be uint8 representation of bool (0=False, other=True)
def find_clusters(image, min_pixels, max_pixels):
    imgH, imgW = image.shape
    clImg = image.copy()
    idx = np.where(clImg>0)

    # return a blank image if no candidate pixels are present
    if len(idx[0]) == 0:
        return np.zeros((imgH,imgW), np.uint8)

    X = np.array([[idx[0][ii], idx[1][ii]] for ii in range(len(idx[0]))])
    clustering = DBSCAN(eps=2, min_samples=min_pixels).fit(X)
    #print("clustering.labels_:", clustering.labels_)
    unique_labels = [x for x in set(clustering.labels_) if x!=-1]
    label_lengths = [len([x for x in clustering.labels_ if x==ul]) for ul in unique_labels]
    if label_lengths:
        max_idx = np.argmax(label_lengths)
        for ilabel in range(len(unique_labels)):
            if label_lengths[ilabel] < max_pixels:
                indexes = [ii for ii in range(len(X)) if clustering.labels_[ii]==ilabel]
                ys = [ X[i][0] for i in indexes ]
                xs = [ X[i][1] for i in indexes ]
                # Display only
                clImg[(ys,xs)] = 2
        clImg[clImg<2] = 0
    return clImg



# find clusters using DBSCAN; ignore clusters of size larger than max_pixel_size
# input image must be uint8 representation of bool (0=False, other=True)
def find_clusters_and_extents(image, min_pixels, max_pixels):
    imgH, imgW = image.shape
    clImg = image.copy()
    idx = np.where(clImg>0)

    extent_dict = {}

    # return a blank image if no candidate pixels are present
    if len(idx[0]) == 0:
        return np.zeros((imgH,imgW), np.uint8), extent_dict

    X = np.array([[idx[0][ii], idx[1][ii]] for ii in range(len(idx[0]))])
    clustering = DBSCAN(eps=2, min_samples=min_pixels).fit(X)
    #print("clustering.labels_:", clustering.labels_)
    unique_labels = [x for x in set(clustering.labels_) if x!=-1]
    label_lengths = [len([x for x in clustering.labels_ if x==ul]) for ul in unique_labels]
    if label_lengths:
        max_idx = np.argmax(label_lengths)
        for ilabel in range(len(unique_labels)):
            if label_lengths[ilabel] < max_pixels:
                indexes = [ii for ii in range(len(X)) if clustering.labels_[ii]==ilabel]
                ys = [ X[i][0] for i in indexes ]
                xs = [ X[i][1] for i in indexes ]
                extent_dict[ilabel] = (min(ys),max(ys),min(xs),max(xs))
                # Display only
                clImg[(ys,xs)] = 2
        clImg[clImg<2] = 0

    ## merge neighboring clusters
    #new_dict = {}
    #for ilabel in extent_dict:
    #    y1,y2,x1,x2 = extent_dict[ilabel]
    #    for jlabel in extent_dict[jlabel]
    #        if jlabel != ilabel:
    #            y11,y22,x11,x22 = extent_dict[jlabel]
    #            ...


    return clImg, extent_dict

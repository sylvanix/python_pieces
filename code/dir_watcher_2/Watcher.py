'''
Watcher - a class to monitor a directory and trigger an appropriate action
          if a file with a newer time is found. This time could be "ctime",
          the time of the most recent metadata change (posix systems),
          "mtime", the time of the most recent content modification,
          "atime", the time of most recent access. It is assumed that
          the file times are comparable. 
'''
import os
import yaml
from pathlib import Path
from threading import Timer
from funcs import data_to_storage
from Logger import Logfile

class Watcher(object):
    def __init__(self, function, *args, **kwargs):
        self._timer     = None
        self.function   = function
        self.interval   = 60
        self.datadir    = 'data'
        self.writedir   = 'csv_store'
        self.datapath   = None
        self.writePath  = None
        self.imworkinhea = False
        self.root_dir = Path(__file__).parent.absolute()
        self._get_settings()
        self._start_logging()
        self.ctime = 0.0
        self.ctime_prev = 0.0
        self.is_running = False
        self._create_dirs()
        self.start()

    ''' checks if already running and issues call to the function '''
    def _run(self):
        self.is_running = False
        self.start()
        if self.imworkinhea == False:
            self.ctime = self.function(self.datadir)
            self._check_latest_ctime()

    ''' start '''
    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    ''' stop watching the dir (used from calling program) '''
    def stop(self):
        self._timer.cancel()
        self.is_running = False
        self.logfile.append('watcher stopped')
        
    ''' compare current time with previous '''    
    def _check_latest_ctime(self):
        if not self.ctime:
            self.ctime = 0.0
        if not self.ctime_prev:
            self.ctime_prev = 0.0
        if self.ctime > self.ctime_prev:
            self.imworkinhea = True
            self._on_newer_ctime()
            self.imworkinhea = False
        self.ctime_prev = self.ctime # we might later want to compare ctime with ctime_prev ...
    
    ''' do this when a newer time is found '''
    def _on_newer_ctime(self):
        self.logfile.append('ingesting new files')
        res = data_to_storage(self.datapath, self.writepath, self.writemethod)
        self.logfile.append(res)

    ''' get settings from the config file '''
    def _get_settings(self):
        params = yaml.safe_load(open('settings.yaml'))
        self.interval = params['interval']
        self.datadir = params['datadir']
        self.writedir = params['writedir']
        self.writemethod = params['writemethod']

    '''  '''
    def _start_logging(self):
        self.logfile = Logfile(self.root_dir, 'starting watcher')

    '''  '''
    def _create_dirs(self):
        self.datapath = Path(self.datadir)
        self.writepath = Path(self.writedir)
        self.datapath.mkdir(parents=True, exist_ok=True)
        self.writepath.mkdir(parents=True, exist_ok=True)
        
        


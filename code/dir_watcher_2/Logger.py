import os
from pathlib import Path
import datetime


''' get_datetime '''
def get_datetime():
    now = datetime.datetime.now()
    return now.strftime("%Y-%m-%d %H:%M:%S")


''' Logfile '''
class Logfile():
    def __init__(self, logdirname, logtext):
        self.subdirpath = Path(logdirname)
        self.filename = os.path.join(self.subdirpath, 'logfile.txt')
        if not os.path.exists(self.filename):
            self.lf = open(self.filename, "w")
        else:
            self.lf = open(self.filename, "a")
        self.lf.write('\n' + get_datetime() + ' - ' + logtext + '\n' )
        self.close_logfile()
    
    def append(self, logtext):
        self.open_logfile()
        self.lf.write( get_datetime() + ' - ' + logtext + '\n' );
        self.close_logfile()

    def open_logfile(self):
        self.lf = open(self.filename, "a")

    def close_logfile(self):
        self.lf.close()

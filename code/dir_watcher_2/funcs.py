'''
    Functions available to the Watcher class.
'''

import os
import pickle
import datetime
from pathlib import Path
import sqlite3


''' Finds the file in the directory with the most recent creation time
    and returns that time in unix epoch seconds '''
def get_latest_file_ctime(dirpath):
    files = [ f for f in os.listdir(dirpath)
              if os.path.isfile(os.path.join(dirpath, f)) ]
    if files:
        ctimes = [Path(os.path.join(dirpath,x)).stat().st_ctime for x in files]
    else:
        ctimes = []
    
    latest = sorted(ctimes)[-1] if ctimes else None
    #print("latest:", latest)
    return latest


'''  '''
def append_data_array_to_csv(data_dict, xfile, filename):
    timestamp = str(xfile).split('.pkl')[0]
    with open(filename, "a") as ap:
        labels = data_dict['labels'].tolist()
        features = data_dict['features'].tolist()
        for ii in range(len(labels)):
            line =  ",".join( [ str(features[ii][0]),
                                str(features[ii][1]),
                                timestamp,
                                str(labels[ii]) ] )
            ap.write( line + '\n' )


'''  '''
def write_to_csv(datadir, pickle_files, file_path):
    res = 0
    header = ",".join(['feature_1', 'feature_2', 'timestamp', 'label'])
    # if the file does not exist, create it
    try:
        if not os.path.isfile(file_path):
            with open(file_path, "w") as ap:
                ap.write(header + '\n') 
    except IOError as e:
        print("error while creating csv file:", e)
        res = 1
        
    # add each pickle file's data to storage
    for xfile in pickle_files:
        print("writing", xfile, "to csv storage ...")
        filepath = Path(os.path.join(datadir, xfile))
        try:
            with open(filepath, 'rb') as f:
                data_dict = pickle.load(f) 
            append_data_array_to_csv(data_dict, xfile, file_path)
        except IOError as e:
            print("error while appending to csv file:", e)
            res = 1
    return res


'''  '''
def write_to_sqlite(datadir, pickle_files, file_path):
    res = 0 # success
    # if database does not exist, create it
    if not os.path.isfile(file_path):
        conn = sqlite3.connect(file_path)
        cur = conn.cursor()
        # Create table
        cur.execute( '''CREATE TABLE from_pickles
                    (feature_1 real, feature_2 real, timestamp text, label integer)''' )
        conn.commit()

    # open the database to insert to the "from_pickles" table
    try:
        conn = sqlite3.connect(file_path)
        cur = conn.cursor()

        # open each pickle file
        for xfile in pickle_files:
            timestamp = str(xfile).split('.pkl')[0]
            print("inserting", xfile, "to database table ...")
            filepath = Path(os.path.join(datadir, xfile))
            with open(filepath, 'rb') as f:
                data_dict = pickle.load(f)

            # insert rows
            labels = data_dict['labels'].tolist()
            features = data_dict['features'].tolist()
            for ii in range(len(labels)):
                thisRow = ( float(features[ii][0]),
                            float(features[ii][1]),
                            timestamp,
                            int(labels[ii]) )
                cur.execute("INSERT INTO from_pickles VALUES (?,?,?,?)", thisRow)

        conn.commit()
    
    except sqlite3.Error as error:
        print("Error while connecting to database:", error)
        res = 1 # failure
    finally:
        if conn:
            conn.close()
    return res


'''  '''
def delete_raw_data(allfiles, data_dir):
    for x in allfiles:
        xpath = Path(os.path.join(data_dir,x))
        res = 1 # failure
        try:
            os.remove( xpath )
            res = 0 # success
        except OSError as e:
            print(f'Error: {xpath} : {e.strerror}')
    return res


'''  '''
def data_to_storage(datadir, writedir, writemethod):

    write_dir = Path(writedir)
    if writemethod.lower() == 'csv':
        storename = 'ingested_data.csv'
    if writemethod.lower() == 'sqlite':
        storename = 'ingested_data.db'

    file_path = Path(os.path.join(write_dir, storename))

    # timestamp
    now = datetime.datetime.now()
    current_time = now.strftime("%Y-%m-%d %H:%M:%S")
    print("\nNew data found: " + current_time)
    file_ext = '.pkl'

    # get all pickle files in the dir
    onlyfiles = [ f for f in os.listdir(datadir)
                  if os.path.isfile(os.path.join(datadir, f)) ]
    pickle_files = sorted([x for x in onlyfiles if x.endswith(file_ext)])

    if writemethod.lower() == 'csv':
        res = write_to_csv(datadir, pickle_files, file_path)
    if writemethod.lower() == 'sqlite':
        res = write_to_sqlite(datadir, pickle_files, file_path)
    if res:
        return "error writing data to store"
    
    res = delete_raw_data(onlyfiles, datadir)
    if res:
        return "data deletion permission error"
    
    return "raw data ingested to storage"

'''
Creates an instance of the Watcher class, which executes a given function
when a new file is created/moved in a given directory. To end the program
press ESC.
'''

import threading
import sys
import time
import Watcher
from funcs import get_latest_file_ctime


watcher = Watcher.Watcher(get_latest_file_ctime)

#''' Now that the Watcher thread is running, we need to be able to stop it 
#    when desired. We will stop it using the ESC key. '''  
exit = threading.Event()
try:
    while(1):
        pass
except KeyboardInterrupt:
    print(' - Keyboard Interrupt: Watcher stopped')
    watcher.stop()
    sys.exit()

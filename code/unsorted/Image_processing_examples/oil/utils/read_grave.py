# -*- coding: utf-8 -*-

__author__  = 'foresterd'
__version__ = '1.0.1'
__date__    = '11 Aug 2017'
__comment__ = 'now returns header, frame headers, meta headers, and image data'

'''
After reading a Polaris Grave file (usually .raw, .dat, or .gv) of length N frames, this module
returns the data in four items:
1. the file header (a Python byte object)
2. the frame headers (an N-element list of Python byte objects)
3. the meta headers (an N-element list of Python byte objects)
3. the image data (an N-element list of Numpy arrays)

Example usage:

header, frameHdrList, metaHdrList, imgDataList = rg.read_grave(infile,
                                                               startFrame=numb_first_frame,
                                                               nFramesToRead=numb_frames,
                                                               verbose=False)

NOTE: the author plans to (soon) implement this module as part of a Python class

'''

import logging
import numpy as np
from os.path import getsize


FILE_HEADER_SIZE = 2048 # size in bytes
NBYTES_DICT = {'uint8':1, 'uint16':2, 'uint32':4, 'float32':4, 'float64':8}

def read_grave(infile, startFrame = 1 ,nFramesToRead = -1, verbose = False):
    # Open file
    try:
        fileIn = open(infile, 'rb')
    except IOError as err:
        logging.debug("Could not open input file %s" % (infile))

    filesize = getsize(infile)
    if verbose: print('filesize =', filesize)

    # Read in header, then rest of binary file
    file_header_dict = {}
    with open(infile, 'rb') as f:
        header = f.read(FILE_HEADER_SIZE)
        #print(header)
        #print('')
        hList = header.decode('utf-8').split('\n')

        # build file header dictionary
        for ii in range(len(hList)):
            if '=' in hList[ii]:
                k,v = hList[ii].split('=')
                file_header_dict[k] = v.split('\r')[0]
        if verbose:
            for k in file_header_dict:
                print(k+':', file_header_dict[k])


        # Read Data Type
        fnfmt = [x.split('FILE_NUMERICAL_FORMAT=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('FILE_NUMERICAL_FORMAT=')]
        if len(fnfmt)>0:
            imgDataType = fnfmt[0]
        else: # if not readable, assume uint16
            imgDataType = 'uint16'

        if imgDataType in ['uint8', 'UINT8']:
            imgDataType = 'uint8'
        if imgDataType in ['FLOAT', 'float', 'float32', 'Float32']:
            imgDataType = 'float32'
        if imgDataType in ['FLOAT64', 'float64', 'Float64']:
            imgDataType = 'float64'

        # Read Image Height
        imgHeight = [x.split('IMAGE_HEIGHT=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('IMAGE_HEIGHT=')]
        if len(imgHeight)>0:
            imgH = int(imgHeight[0])
        else:
            imgH = 0

        # Read Image Width
        imgWidth = [x.split('IMAGE_WIDTH=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('IMAGE_WIDTH=')]
        if len(imgWidth)>0:
            imgW = int(imgWidth[0])
        else:
            imgW = 0

        # Read Image Depth
        imgDepth = [x.split('IMAGE_DEPTH=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('IMAGE_DEPTH=')]
        if len(imgDepth)>0:
            depth = int(imgDepth[0])
        else:
            depth = 1 # defaults to 1 color layer


        # Read FRAME_HEADER_SIZE in bytes
        frhsize = [x.split('FRAME_HEADER_SIZE=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('FRAME_HEADER_SIZE=')]
        if len(frhsize)>0:
            frhsize = int(frhsize[0])
        else:
            frhsize = 0

        # Read META_HEADER_SIZE in bytes
        metasize = [x.split('META_HEADER_SIZE=')[-1].split('\r')[0]
                     for x in hList
                     if x.startswith('META_HEADER_SIZE=')]
        if len(metasize)>0:
            metasize = int(metasize[0])
        else:
            metasize = 0

        nbytes = NBYTES_DICT[imgDataType]
        frameHdrList = []
        metaHdrList = []
        imgList = []
        offset = FILE_HEADER_SIZE
        if verbose: print('filesize', 'imgW', 'imgH', 'depth', 'nbytes', 'frhsize', 'metasize')
        if verbose: print(filesize, imgW, imgH, depth, nbytes, frhsize, metasize)
        totalFrames = int( (filesize - FILE_HEADER_SIZE)/((imgH*imgW*depth*nbytes)+frhsize+metasize) )
        if verbose: print('total frames:', totalFrames)

        if nFramesToRead == -1:
            nFramesToRead = totalFrames

        offset += (startFrame -1) * (frhsize + metasize + (imgH * imgW * depth * nbytes))

        for ii in range(nFramesToRead):

            if frhsize > 0:
                frmHdr = np.memmap(f, dtype='uint8', mode='r', offset=offset, shape=frhsize).tolist()
                frameHdrList.append(bytes(frmHdr.copy()).decode())
                offset += frhsize

            if metasize > 0:
                mtaHdr = np.memmap(f, dtype='uint8', mode='r', offset=offset, shape=metasize)
                metaHdrList.append(bytes(mtaHdr.copy()).decode())
                offset += metasize

            frame = np.memmap(f, dtype=imgDataType, mode='r', offset=offset, shape=(imgH*imgW*depth))
            if depth > 1:
                frame = frame.reshape(imgH, imgW, depth)
            else:
                frame = frame.reshape(imgH, imgW)

            imgList.append(frame.copy())
            offset += imgH * imgW * depth * nbytes
        return (header, frameHdrList, metaHdrList, imgList)


###############################################################################
# USE THE ABOVE MODULE LIKE THIS:
'''
fileHdr, frameHdrList, metaHdrList, imgDataList = rg.read_grave(infile,
                                                                startFrame=numb_first_frame,
                                                                nFramesToRead=numb_frames,
                                                                verbose=True)

# Then, for example, run a program from command line like:
$ python3 program_to_read_images.py 100 200 10 -p
'''

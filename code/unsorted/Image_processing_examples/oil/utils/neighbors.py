import numpy as np


'''
Returns a list of neighboring point values, out to max_sigma distance from (0,0).
These values will be added to a (y,x) point from a map.
'''
def getAdjacency(max_sigma):
    span = [x for x in range(-(max_sigma), (max_sigma)+1)]
    adjacency = [(i,j) for i in span for j in span if (i,j) != (0,0)]
    return adjacency


'''
Keep only non-zero elements of map having at least nbNeighborsRequired neighbors at distance=1.
'''
def removeUnconnected(mapImg, nbNeighborsRequired):
    rows, cols = mapImg.shape
    idx = np.where(mapImg == 1) # (array([2, 2, 3, 6]), array([2, 3, 2, 7]))
    adjacency = getAdjacency(1) # 
    for ii in range(len(idx[0])):
        #print( idx[0][ii], idx[1][ii] )
        neighbors = ([(x[0]+idx[0][ii], x[1]+idx[1][ii]) for x in adjacency])
        # remove any cells that would lie outside the array
        combined = [x for x in neighbors if (0<x[0]<rows) and (0<x[1]<cols)]
        # are any of these connected indexes included in mapImg?
        mapXYs = [(idx[0][ii],idx[1][ii]) for ii in range(len(idx[0]))]
        candidates = [x for x in mapXYs if x in combined]
        if len(candidates) < nbNeighborsRequired:
            mapImg[idx[0][ii], idx[1][ii]] = 0

    return mapImg


'''
Replace valued for known "bad-pixels" with the mean value of their non-bad neighbors
'''
def getReplacementValue(ngidx, ii, Ximg, adjacency_distance):
    rows, cols = Ximg.shape
    adjacency = getAdjacency(adjacency_distance)
    neighbors = ([(x[0]+ngidx[ii][0], x[1]+ngidx[ii][1]) for x in adjacency])
    # remove any cells that would lie outside the array
    combined = [x for x in neighbors if (0<x[0]<rows) and (0<x[1]<cols)]
    # remove any cells also in ngidx
    combined = [x for x in combined if (x[0], x[1]) not in ngidx]
    replVal = np.mean(np.array([Ximg[x[0],x[1]] for x in combined]))
    return replVal    
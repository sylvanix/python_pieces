from os.path import getsize
import logging

FILE_HEADER_SIZE = 2048 # size in bytes
NBYTES_DICT = {'uint8':1, 'uint16':2, 'uint32':4, 'float32':4, 'float64':8}

def getFileHeaderInfo(infile, verbose=False):
    
    # Open file
    try:
        fileIn = open(infile, 'rb')
    except IOError as err:
        logging.debug("Could not open input file %s" % (infile))

    filesize = getsize(infile)
    if verbose: print('filesize =', filesize)

    # Read in header, then rest of binary file
    file_header_dict = {}
    with open(infile, 'rb') as f:
        header = f.read(FILE_HEADER_SIZE)
        #print(header)
        #print('')
        hList = header.decode('utf-8').split('\n')
    
    
    # Read Data Type
    fnfmt = [x.split('FILE_NUMERICAL_FORMAT=')[-1].split('\r')[0]
                 for x in hList
                 if x.startswith('FILE_NUMERICAL_FORMAT=')]
    if len(fnfmt)>0:
        imgDataType = fnfmt[0]
    else: # if not readable, assume uint16
        imgDataType = 'uint16'

    if imgDataType in ['uint8', 'UINT8']:
        imgDataType = 'uint8'
    if imgDataType in ['FLOAT', 'float', 'float32', 'Float32']:
        imgDataType = 'float32'
    if imgDataType in ['FLOAT64', 'float64', 'Float64']:
        imgDataType = 'float64'

    # Read Image Height
    imgHeight = [x.split('IMAGE_HEIGHT=')[-1].split('\r')[0]
                 for x in hList
                 if x.startswith('IMAGE_HEIGHT=')]
    if len(imgHeight)>0:
        imgH = int(imgHeight[0])
    else:
        imgH = 0

    # Read Image Width
    imgWidth = [x.split('IMAGE_WIDTH=')[-1].split('\r')[0]
                 for x in hList
                 if x.startswith('IMAGE_WIDTH=')]
    if len(imgWidth)>0:
        imgW = int(imgWidth[0])
    else:
        imgW = 0

    # Read Image Depth
    imgDepth = [x.split('IMAGE_DEPTH=')[-1].split('\r')[0]
                 for x in hList
                 if x.startswith('IMAGE_DEPTH=')]
    if len(imgDepth)>0:
        depth = int(imgDepth[0])
    else:
        depth = 1 # defaults to 1 color layer


    # Read FRAME_HEADER_SIZE in bytes
    frhsize = [x.split('FRAME_HEADER_SIZE=')[-1].split('\r')[0]
                 for x in hList
                 if x.startswith('FRAME_HEADER_SIZE=')]
    if len(frhsize)>0:
        frhsize = int(frhsize[0])
    else:
        frhsize = 0

    # Read META_HEADER_SIZE in bytes
    metasize = [x.split('META_HEADER_SIZE=')[-1].split('\r')[0]
                 for x in hList
                 if x.startswith('META_HEADER_SIZE=')]
    if len(metasize)>0:
        metasize = int(metasize[0])
    else:
        metasize = 0
        
    nbytes = NBYTES_DICT[imgDataType]
    if verbose: print('filesize', 'imgW', 'imgH', 'depth', 'nbytes', 'frhsize', 'metasize')
    if verbose: print(filesize, imgW, imgH, depth, nbytes, frhsize, metasize)
    totalFrames = int( (filesize - FILE_HEADER_SIZE)/((imgH*imgW*depth*nbytes)+frhsize+metasize) )
    if verbose: print('total frames:', totalFrames)     
        
    return (imgDataType, imgH, imgW, depth, frhsize, metasize, nbytes, totalFrames )
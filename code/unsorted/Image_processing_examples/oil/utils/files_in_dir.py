import os
import re

'''
   returns a file path generator for all files under data_dir
'''
def get_imgs(data_dir):
    fileiter = (os.path.join(root, f)
                for root, _, files in os.walk(data_dir)
                for f in files)
    return fileiter

'''
   to use with Python's sort() key option
'''
_nsre = re.compile('([0-9]+)')
def natural_sort_key(s):
    return [int(text) if text.isdigit() else text.lower()
            for text in re.split(_nsre, s)]

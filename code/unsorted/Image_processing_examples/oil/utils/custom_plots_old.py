import matplotlib.pyplot as plt
import numpy as np
import warnings
from skimage.color import gray2rgb
from skimage import img_as_ubyte


warnings.filterwarnings("ignore", category=UserWarning)
plt.style.use('dark_background')
# https://matplotlib.org/examples/color/named_colors.html
colors = {1:'deepskyblue', 2:'lightskyblue', 3:'limegreen', 4:'orangered',
          5:'red', 6:'gold', 7:'snow', 8:'black'}

fc='black'
cm='gray'
theme_col = colors[6]
fig, ax = plt.subplots(1, 1, figsize=(8.75,7))

#fig, ax = plt.subplots(1, 1, figsize=(18,5)) # 1 x 3

#fig, ax = plt.subplots(1, 1, figsize=(18,10)) # 2 x 3

#fig, ax = plt.subplots(1, 1, figsize=(12.25,9.8)) # 2 x 2

#fig, ax = plt.subplots(1, 1, figsize=(17.5,7)) # 1 x 2

fig.patch.set_facecolor(fc)
fig.suptitle(" ")


def show_img_fixed(img, name, cm, minval, maxval, ask=True):
    plt.ion()
    plt.imshow(img, cmap=cm, vmin=minval, vmax=maxval)
    plt.show()
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    plt.tight_layout()
    plt.draw()
    if ask:
        accept = input('OK? ')
    else:
        accept = 'y'
        plt.pause(0.0001)
    plt.cla()
    return(accept)


def show_img_return_input(img, name, cm='gray', ask=True):
    plt.ion()
    plt.imshow(img, cmap=cm)
    plt.show()
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    
    # # text labels for a 1 x 3 image plot
    # ax.text(10, 50, 'V_Delta', color='green', fontsize=23)
    # ax.text(10+640, 50, 'S1_Delta', color='green', fontsize=23)
    # ax.text(10+640+10+640, 50, 'Detections', color='green', fontsize=23)    
    
    plt.tight_layout()
    plt.draw()
    if ask:
        accept = input('OK? ')
    else:
        accept = 'y'
        plt.pause(0.0001)
    plt.cla()
    return(accept)



def show_img_with_detections(img, name, mask,
                             det_map, conf_map, is_confirmed,
                             nYregions, nXregions, ask=True):
    if is_confirmed:
            fig.suptitle(is_confirmed)                         
    img = img_as_ubyte(img)
    redVal = 255 # 0 : 255
    blueVal = 255
    rows, cols = img.shape
    dy = rows//nYregions
    dx = cols//nXregions
    imgDisp = gray2rgb(img)
    for iy in range(nYregions):
        for ix in range(nXregions):
            y_0 = iy*dy
            x_0 = ix*dx
            if det_map[iy,ix] == 1:
                imgDisp[y_0:(y_0+dy), x_0:(x_0+dx), 0] = redVal
            if conf_map[iy,ix] == 1:
                imgDisp[y_0:(y_0+dy), x_0:(x_0+dx), 0] = 0
                imgDisp[y_0:(y_0+dy), x_0:(x_0+dx), 2] = blueVal
            imgDisp[:,:,2] *= mask            
    
    plt.ion()
    plt.imshow(imgDisp)
    plt.show()
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    
    # spacing for gridlines
    ax.set_yticks(np.arange(0, rows, rows//nYregions))
    ax.set_xticks(np.arange(0, cols, cols//nXregions))
    
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    #plt.tight_layout()
    plt.grid(color='gray')
    plt.xticks(rotation='vertical')
    plt.draw()
    if ask:
        accept = input('OK? ')
    else:
        accept = 'y'
        plt.pause(0.0001)
    plt.cla()
    return(accept)
    


def write_img_with_detections(img, name, imgFileNb, mask,
                              det_map, conf_map, is_confirmed,
                              nYregions, nXregions, out_dir):              
    if is_confirmed:
            fig.suptitle(is_confirmed)                                
    img = img_as_ubyte(img)
    redVal = 255 # 0 : 255
    blueVal = 255
    rows, cols = img.shape
    dy = rows//nYregions
    dx = cols//nXregions
    imgDisp = gray2rgb(img)
    for iy in range(nYregions):
        for ix in range(nXregions):
            y_0 = iy*dy
            x_0 = ix*dx
            if det_map[iy,ix] == 1:
                imgDisp[y_0:(y_0+dy), x_0:(x_0+dx), 0] = redVal 
            if conf_map[iy,ix] == 1:
                imgDisp[y_0:(y_0+dy), x_0:(x_0+dx), 0] = 0
                imgDisp[y_0:(y_0+dy), x_0:(x_0+dx), 2] = blueVal
            imgDisp[:,:,2] *= mask                       
    
    plt.ion()
    plt.imshow(imgDisp)
    plt.show()
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    
    # spacing for gridlines
    ax.set_yticks(np.arange(0, rows, rows//nYregions))
    ax.set_xticks(np.arange(0, cols, cols//nXregions))
    
    if (cols//nXregions) < 30:
        ax.set_xticklabels(np.arange(0, nXregions, nXregions))
    
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    #plt.tight_layout()
    plt.grid(color='gray')
    plt.savefig(out_dir+'/'+imgFileNb, bbox_inches='tight')
    plt.cla()



def write_img(img, iframeStr, titleStr, out_dir, cm='gray'):
    plt.ion()
    #plt.imshow(img, cmap=cm, vmin=vmin, vmax=vmax)
    plt.imshow(img, cmap=cm)
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    #plt.xticks([])
    plt.xticks([])
    plt.yticks([])
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_title(titleStr)
    ax.title.set_color(theme_col)
    
    # text labels for a 1 x 2 image plot
    # ax.text(10, 50, 'S1', color='lightgreen', fontsize=23)
    # ax.text(10+640, 50, 'S1_Delta', color='lightgreen', fontsize=23)
    
    # # text labels for a 1 x 3 image plot
    # ax.text(10, 50, 'V_Delta', color='green', fontsize=23)
    # ax.text(10+640, 50, 'S1_Delta', color='green', fontsize=23)
    # ax.text(10+640+10+640, 50, 'Detections', color='green', fontsize=23)
    
    # # text labels for a 1 x 3 image plot
    # ax.text(10, 50, 'V_Delta', color='green', fontsize=23)
    # ax.text(10+640, 50, 'S1_Delta', color='green', fontsize=23)
    # ax.text(10+640+10+640, 50, 'Detections', color='green', fontsize=23)  
    
    # # text labels for a 2 x 3 image plot
    # ax.text(10, 50, 'H', color='lightgreen', fontsize=23)
    # ax.text(10+640, 50, 'V', color='lightgreen', fontsize=23)
    # ax.text(10+640+10+640, 50, 'hv_mask', color='lightgreen', fontsize=23)
    # ax.text(10, 50+512, 'V_Delta', color='lightgreen', fontsize=23)
    # ax.text(10+640, 50+512, 'S1_Delta', color='lightgreen', fontsize=23)
    # ax.text(10+640+10+640, 50+512, 'Detections', color='lightgreen', fontsize=23)    
    
    plt.savefig(out_dir+'/'+iframeStr, bbox_inches='tight')
    plt.cla()
    
    
############################################################################################    
        
'''
WITH KEYBOARD INTERRUPT


USAGE:

DISPLAYING = True
count = 0
while (DISPLAYING):
    if count == 0:
        initial_cid = custom_plots.show_img_kb(img, 'test')
    else
        cid = custom_plots.show_img_kb(img, 'test')
        if cid != initial_cid:
            DISPLAYING = False
    count += 1

'''

'''
 onkeypress()
 a function taking input from the keyboard to change settings or quit the program
'''
cid_dict = {}
def onkeypress(event):

    if event.key in ['', 'q']:
        print('Terminating program')
        plt.gcf().canvas.mpl_disconnect(cid_dict['cid'])
        del cid_dict['cid']
    elif event.key == 'shift':
        pass
    else:
        print('Press "q" to quit, "i" to increment, or "d" to decrement.')


def show_img_kb(img, name, cm='gray'):
    plt.ion()
    plt.imshow(img, cmap=cm)
    plt.show()
    cid_dict['cid'] = plt.gcf().canvas.mpl_connect('key_press_event', onkeypress)
    ax.spines['bottom'].set_color(theme_col)
    ax.spines['top'].set_color(theme_col)
    ax.spines['right'].set_color(theme_col)
    ax.spines['left'].set_color(theme_col)
    ax.tick_params(axis='x', colors=theme_col)
    ax.tick_params(axis='y', colors=theme_col)
    ax.yaxis.label.set_color(theme_col)
    ax.xaxis.label.set_color(theme_col)
    if type(name)==int:
        ax.set_title('frame '+str(name))
    else:
        ax.set_title(name)
    ax.title.set_color(theme_col)
    plt.tight_layout()
    plt.draw()
    plt.pause(0.0001)
    plt.cla()
    ax.cla()
    if cid_dict:
        return(cid_dict['cid'])
    else:
        return(-1)

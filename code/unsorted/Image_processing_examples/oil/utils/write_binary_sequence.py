# -*- coding: utf-8 -*-

'''
This module writes a grave file with a file header followd by N frames. Each frame contains:
* the frame header (Optional) (a Python byte object)
* the meta header (Optional)  (a Python byte object)
* the flattened images        (a Numpy array)

Example usage:

>>> wg.write_grave(outfile, frameHdrList, metaHdrList, imgDataList)


READ BACK WHAT YOU HAVE WRITTEN LIKE THIS:

import numpy as np
import read_grave as rg

infile = 'outfile.raw'

numb_first_frame = 1
numb_frames = 5
fileHdr, frameHdrList, metaHdrList, imgDataList = rg.read_grave(infile,
                                                                startFrame=numb_first_frame,
                                                                nFramesToRead=numb_frames,
                                                                verbose=False)

'''


import struct
import numpy as np

file_header_size = 2048

def write_grave(filename, frame_header_list, meta_header_list, img_data_list):
    fout = filename + '.raw'
    
    print('number of frames:', len(img_data_list))
    
    frame_header_size = 0
    if frame_header_list:
        # find the longest string of all elements in frame_header_list
        frame_header_size = max([len(x) for x in frame_header_list])
        
    meta_header_size = 0
    if meta_header_list:
        # find the longest string of all elements in meta_header_list
        meta_header_size = max([len(x) for x in meta_header_list])        
    
    # read image shape
    depth = 1
    shape = img_data_list[0].shape
    if len(shape)==3:
        rows, cols, depth = img_data_list[0].shape
    else:
        rows, cols = img_data_list[0].shape
        
    frame = np.reshape(img_data_list[0], rows*cols*depth)
    print('frame.dtype:', frame.dtype)
    if frame.dtype == 'uint8':
        fmt = str(frame.shape[0])+'B'
    elif frame.dtype == 'uint16':
        fmt = str(frame.shape[0])+'H'
    elif frame.dtype == 'float32':
        fmt = str(frame.shape[0])+'f'        
    elif frame.dtype == 'float64':
        fmt = str(frame.shape[0])+'d'
    else:
        fmt = str(frame.shape[0])+'I'        
        
    file_fmt = str(frame.dtype)
    img_height = str(rows)
    img_width = str(cols)
    hdr_num_size = str(frame_header_size)
        
    file_header = str.encode(
                             '0000002048\r\n' +
                             'FILE_DATA_BEGIN=0000002048\r\n' +
                             'FILE_BYTE_ORDERING=LITTLE-ENDIAN\r\n' +
                             'FILE_NUMERICAL_FORMAT='+file_fmt+'\r\n' +
                             'FRAME_HEADER_SIZE='+hdr_num_size+'\r\n' +
                             'IMAGE_ADJUST_INCREMENT=1\r\n' +
                             'IMAGE_ADJUST_RESOLUTION=0\r\n' +
                             'IMAGE_HEIGHT='+img_height+'\r\n' +
                             'IMAGE_MAX_PIXEL_VALUE=65535'+'\r\n' +
                             'IMAGE_MIN_PIXEL_VALUE=0'+'\r\n' +
                             'IMAGE_WIDTH='+img_width+'\r\n'
                             )
                                                        
    with open(fout, 'wb') as outfile:
    
        # write the file header
        outfile.write(file_header)
        remaining = 2048 - len(file_header)
        padding = str.encode(' '*remaining)
        outfile.write(padding)
    
        # iterate through the lists, writing items
        for ii in range(len(img_data_list)):
    
            # write this frame header
            if frame_header_list:
                frame_header = str.encode(frame_header_list[ii])
                outfile.write(frame_header)
                remaining = frame_header_size - len(frame_header)
                padding = str.encode(' '*remaining)
                outfile.write(padding)
                
            # write this meta header
            if meta_header_list:
                meta_header = str.encode(meta_header_list[ii])
                outfile.write(meta_header)
                remaining = meta_header_size - len(meta_header)
                padding = str.encode(' '*remaining)
                outfile.write(padding)                
    
            # write this image data
            frame = np.reshape(img_data_list[ii], rows*cols*depth)
            binframe = struct.pack(fmt, *frame)
            print('writing image', ii, '   size:', frame.shape)
            outfile.write(binframe)

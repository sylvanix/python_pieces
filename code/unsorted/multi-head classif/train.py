'''
Trains classifier + regression model for thing
__comment__ = 'fine-tuning on top of ImageNet + Regression'
'''

import os
import json
import argparse
import numpy as np

# suppress the TensorFlow warning
from os import environ
environ['TF_CPP_MIN_LOG_LEVEL']='2'

from utils import u_s_copies


####################################################################################
# Argument parser
####################################################################################
parser = argparse.ArgumentParser()
parser.add_argument('epochs', type=int,
                    help='set number of training epochs')
parser.add_argument('-c', '--continuing', action='store_true',
                    help='continue training the saved model')
parser.add_argument('-s', '--showing', action='store_true',
                    help='show some training images')
args = parser.parse_args()
N_epochs = args.epochs


####################################################################################
# Load data
####################################################################################
x_train_npz = './data/split/x_train.npy'
y_train_npz = './data/split/y_train.npy'
x_val_npz = './data/split/x_val.npy'
y_val_npz = './data/split/y_val.npy'

x_train = np.load(x_train_npz).astype('float32')
y_train = np.load(y_train_npz).astype('float32')
x_val = np.load(x_val_npz).astype('float32')
y_val = np.load(y_val_npz).astype('float32')

print('x_train:', x_train.shape, 'y_train:', y_train.shape)
print('x_val:', x_val.shape, 'y_val:', y_val.shape)

# normalize images (divide by 255)
x_train /= 255.
x_val /= 255.

y_train_clf, y_train_row, y_train_col, y_train_rad = ( y_train[:,0:4], y_train[:,4], y_train[:,5], y_train[:,6] )
y_val_clf, y_val_row, y_val_col, y_val_rad = ( y_val[:,0:4], y_val[:,4], y_val[:,5], y_val[:,6] )

# normalize the regression target data (rows, cols, radius)
y_train_row /= 63.
y_train_col /= 63.
y_train_rad /= 63.
y_val_row /= 63.
y_val_col /= 63.
y_val_rad /= 63.

####################################################################################
# Train a four-output model from the pre-trained models
####################################################################################
from keras.applications import VGG16
from keras import layers
from keras import Input
from keras.models import Model

numb_images, rows, cols, chans = x_train.shape
batch_size = 32

# the convolutional base of ImageNet trained on VGG16 (all trainable)
conv_base = VGG16(weights='imagenet', include_top=False, input_shape=(rows, cols, chans))
#conv_base.summary()

# the input format of the images
image_input = Input(shape=(rows, cols, chans), name='image_input')
conv_base_output = conv_base(image_input)

# add the fully-connected layers
x = layers.Flatten(name='flatten')(conv_base_output)
x = layers.Dense(256, activation='relu', name='fc1')(x)

# define the four output layers
class_prediction = layers.Dense(4, activation='softmax', name='class_pred')(x)
row_regression = layers.Dense(1, name='row_pred')(x)
col_regression = layers.Dense(1, name='col_pred')(x)
radius_regression = layers.Dense(1, name='radius_pred')(x)

model = Model(image_input, [class_prediction, row_regression,
                            col_regression, radius_regression])

# compile the model
from keras.optimizers import Adam
opt = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
model.compile( optimizer=opt,
               loss={ 'class_pred': 'categorical_crossentropy',
                      'row_pred': 'mse',
                      'col_pred':'mse',
                      'radius_pred':'mse' },
               loss_weights={ 'class_pred':5.0,
                              'row_pred':1.0,
                              'col_pred':1.0,
                              'radius_pred':2.5 } )

# to continue training from a previous model
if args.continuing:
    print('continuing training of saved model...')
    model.load_weights('./model/model.h5')

#model.summary()

history = model.fit( x_train, [y_train_clf, y_train_row, y_train_col, y_train_rad],
                     epochs=N_epochs, batch_size=batch_size,
                     validation_data=(x_val, [y_val_clf, y_val_row, y_val_col, y_val_rad]) )

# save trained model weights and architecture
model.save_weights('./model/model.h5', overwrite=True)
with open('model/model.json', 'w') as outfile:
    json.dump(model.to_json(), outfile)

################################################################################
# Plot the loss and accuracy training curves
################################################################################
# for x in history.history:
#     print(x)
# val_loss
# val_class_pred_loss
# val_row_pred_loss
# val_col_pred_loss
# val_radius_pred_loss
# loss
# class_pred_loss
# row_pred_loss
# col_pred_loss
# radius_pred_loss

import matplotlib.pyplot as plt
from utils import smooth_curve as smooth

loss = history.history['loss']
class_loss = history.history['class_pred_loss']
rows_loss = history.history['row_pred_loss']
cols_loss = history.history['col_pred_loss']
radius_loss = history.history['radius_pred_loss']

loss_val = history.history['val_loss']
class_val = history.history['val_class_pred_loss']
rows_val = history.history['val_row_pred_loss']
cols_val = history.history['val_col_pred_loss']
radius_val = history.history['val_radius_pred_loss']

epochs = range(len(loss))

# smoothed curves ---------------------------------------------------
plt.figure()
plt.plot(epochs, smooth.smooth_curve(loss), 'ko', label='train loss')
plt.plot(epochs, smooth.smooth_curve(class_loss), 'bo', label='class')
plt.plot(epochs, smooth.smooth_curve(rows_loss), 'ro', label='row')
plt.plot(epochs, smooth.smooth_curve(cols_loss), 'mo', label='col')
plt.plot(epochs, smooth.smooth_curve(radius_loss), 'go', label='radius')

plt.plot(epochs, smooth.smooth_curve(loss_val), 'k')
plt.plot(epochs, smooth.smooth_curve(class_val), 'b')
plt.plot(epochs, smooth.smooth_curve(rows_val), 'r')
plt.plot(epochs, smooth.smooth_curve(cols_val), 'm')
plt.plot(epochs, smooth.smooth_curve(radius_val), 'g')

plt.title('Training and validation losses')
plt.legend(loc='upper right')
plt.savefig('training_and_val_loss.png')
plt.show()

input file: Data/image_dir/2020-06-15_22-36-59_270/270.jpg

Local Time hour: 22
Local Time minute: 36
Local Time second: 59

***** Greenwich Time ****
day: 16
month: 6
year: 2020
hour: 3
minute: 36
second: 59

pitch: 5.177
roll: -0.485
longitude: -87.476
use center pixel: TRUE
use tilt correction: FALSE

number of stars: 18

Apparent Greenwich Sidereal Time: 319.115

***** StarChannel results *****
   right ascension (deg.): 231.639
       declination (deg.): 29.3077
    rotation angle (deg.): -89.9077

***** Brightest Stars Identified *****
The star ωBoo (524.65, 1178.48)
The star ψBoo (725.15, 1113.69)
The star 45Boo (505.54, 1055.70)
The star 46Boo (654.37, 1024.14)
The star χBoo (948.54, 874.50)
The star δBoo (1380.16, 841.83)
The star οCrB (993.28, 745.49)
The star 50Boo (1338.16, 704.82)
The star ηCrB (1062.49, 675.88)
The star Alkalurops (μ1Boo) (1804.57, 644.78)
The star Nusakan (βCrB) (939.60, 571.54)
The star θCrB (1174.96, 459.23)
The star Alphecca / Gemma (αCrB) (692.57, 410.84)
The star ζ2CrB (1731.04, 331.82)
The star γCrB (653.34, 221.51)
The star πCrB (1302.37, 217.79)
The star δCrB (635.55, 59.41)
The star κCrB (1640.04, 75.85)

***** Constellations Identified *****
Part of the constellation Corona Borealis (CrB)

solved in 878 microseconds.

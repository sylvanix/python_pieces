import os, csv
from pathlib import Path
import subprocess
from tkinter import filedialog

'''
Fields for the CSV file with example entries:
############################################
date                     | 2020-04-23
time CST                 | 20:03:32
camera                   | PointGrey
lens                     | Fujion
description              | Flat_1
imgId                    | 270
heading                  | -90.2253
lat                      | 35.1669
lon                      | -86.8402
exposure                 | 6060
gain                     | 30
focal_len                | 35
f_number                 | ?
pitch                    | 0.832
roll                     | 0.225
number_bright_stars      | 20
true rotation            | 270
true latitude            | 24.7304
true longitude           | -86.5861
distortion_corrected     | FALSE
use_center_px            | FALSE
center_px_0              | 470.7567
center_px_1              | 670.2639
use_pitch_roll_transform | TRUE
use_ahrs_calibration     | FALSE
calc_succeeded           | TRUE
solve_time               | 739
'''

header = ["date", "time CST", "camera", "lens", "description", "imgId",
          "heading", "lat", "lon", "exposure", "gain", "focal_len", "f_number",
          "pitch", "roll", "number_bright_stars", "true rotation", "true latitude",
          "true longitude", "distortion_corrected", "use_center_px", "center_px_0",
          "center_px_1", "use_heading_correction", "calc_succeeded", "solve_time"]

#rootDir = "/home/davros/testing/Skypass/test_astrometry/Data"
rootDir = "/home/davros/Desktop/results"

dirpath = filedialog.askdirectory(initialdir=rootDir)
print(dirpath)
run_dir_name = dirpath.split('/')[-1]
print("run_dir_name:", run_dir_name)
#dirpath = rootDir
#p = Path(rootDir)
p = Path(dirpath)
subdirs = [x for x in p.iterdir() if x.is_dir()]

date = []
time = []
localtime = []
heading = []
lat = []
lon = []
pitch = []
roll = []
nb_stars = []
use_cp = []
cp_0 = []
cp_1 = []
use_tilt_correction = []
calc_succeeded = []
true_heading = []
solve_time = []
for subdir in subdirs:
    p = Path(subdir)
    corrFiles = [x for x in p.iterdir() if ( x.is_file() and x.name.endswith(".corr") )]
    SUCCEEDED = "TRUE" if len(corrFiles) > 0 else "FALSE"
    calc_succeeded.append(SUCCEEDED)
    print(    "SUCCEEDED:", SUCCEEDED)

    if SUCCEEDED:
        wcsFiles = [x for x in p.iterdir() if ( x.is_file() and x.name.endswith(".wcs") )]
        if len(wcsFiles) > 0:
            wcsFile = wcsFiles[0]
            command = "wcsinfo " + str(wcsFile)
            res = subprocess.getoutput(command).split('\n')[0:2]
            crpix0 = str(round(float(res[0].split(" ")[-1]),2))
            crpix1 = str(round(float(res[1].split(" ")[-1]),2))
            cp_0.append(crpix0)
            cp_1.append(crpix1)
        else:
            cp_0.append("")
            cp_1.append("")
    
    resFiles = [x for x in p.iterdir() if ( x.is_file() and x.name == "results.txt" )]
    if len(resFiles) > 0:
        resFile = resFiles[0]
        print(resFile)
        with open(resFile, "r") as f:
            reader = csv.reader(f, delimiter=",")
            day = ""
            month = ""
            year = ""
            hour = ""
            minute = ""
            second = ""
            localhour = ""
            localminute = ""
            localsecond = ""
            dmy = True
            hms = True
            lhms = True
            for i, line in enumerate(reader):
                #print(i, line)
                if line: # if line not empty

                    # true heading from filename
                    if line[0].startswith('input file:'):
                        true_heading.append(line[0].split('/')[-1].split('h')[-1].split('.')[0])

                    # Date (Greenwich)
                    if line[0].startswith('day'):
                        day = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('month'):
                        month = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('year'):
                        year = (line[0].split(': ')[-1])
                    if (day and month and year and dmy):
                        date.append('-'.join([year, month, day]))
                        dmy = False

                    # Time (Greenwith)
                    if line[0].startswith('hour'):
                        hour = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('minute'):
                        minute = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('second'):
                        second = (line[0].split(': ')[-1]).zfill(2)
                    if (hour and minute and second and hms):
                        time.append(':'.join([hour, minute, second]))
                        hms = False

                    # Time (Local)
                    if line[0].startswith('Local Time hour'):
                        localhour = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('Local Time minute'):
                        localminute = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('Local Time second'):
                        localsecond = (line[0].split(': ')[-1]).zfill(2)
                    if (localhour and localminute and localsecond and lhms):
                        localtime.append(':'.join([localhour, localminute, localsecond]))
                        lhms = False

                    # Heading
                    if 'rotation angle' in line[0]:
                        heading.append(line[0].split(': ')[-1])
                        
                    # Latitude
                    if 'declination' in line[0]:
                        lat.append(line[0].split(': ')[-1])

                    # Longitude
                    if 'longitude' in line[0]:
                        lon.append(line[0].split(': ')[-1])    
                        
                    # Pitch
                    if 'pitch: ' in line[0]:
                        pitch.append(line[0].split(': ')[-1])
                        
                    # Roll
                    if 'roll: ' in line[0]:
                        roll.append(line[0].split(': ')[-1])
                        
                    # Number of Bright Stars
                    if 'number of stars' in line[0]:
                        nb_stars.append(line[0].split(': ')[-1])

                    # Use Center Pixel
                    if 'use center pixel: ' in line[0]:
                        use_cp.append(line[0].split(': ')[-1])

                    # Use tilt correction
                    if 'use tilt correction: ' in line[0]:
                        use_tilt_correction.append(line[0].split(': ')[-1])

                    # solve time (millisec)
                    if 'solved in ' in line[0]:
                        solve_time.append(line[0].split(' ')[2])
                        

    else:
        "No Results.txt file exists"

# write results to a CSV file
outpath = Path(dirpath, run_dir_name+".csv")
with open(outpath, "a") as ap:
    if (os.path.getsize(outpath)) <= 0:
        wr = csv.writer(ap)
        wr.writerow(header)
        for i in range(len(date)):
            wr.writerow( [ date[i], localtime[i], "", "", "", "", heading[i], lat[i], lon[i],
                           "", "", "", "", pitch[i], roll[i], nb_stars[i], true_heading[i],
                           "34.7304", "-86.5861", "", use_cp[i], cp_0[i], cp_1[i],
                           use_tilt_correction[i], calc_succeeded[i], solve_time[i] ] )
        ap.close()

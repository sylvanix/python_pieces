import os, csv
from pathlib import Path
import subprocess
from tkinter import filedialog

trueLat = "34.7304";
trueLon = "-86.5861";

header = [
# parsed from filename
"date", "time CST", "camera", "lens", "description", "exposure", "gain",
"pitch", "roll", "rotation angle",
# known position
"true lat","true lon",
# calculated from Astrometry.net & Meeus
"AGSiderealTime", "Dec", "RA", "heading", "lat", "lon",
# positon-pitch-roll corrections
"pitch_corr", "roll_corr", "projectionAngle", "heading_corr", "Dec_corr",
"RA_corr", "lat_corr", "lon_corr",
# other useful stuff
"nb_bright_stars", "cp-x", "cp-y", "use_center-pixel", "use_tilt_corr",
"calc_succeeded", "solve time" ]
        
rootDir = "/home/davros/testing/Skypass/image_solver/Data"
#rootDir = "/home/davros/testing/Skypass/Python/make_csv/results"

dirpath = filedialog.askdirectory(initialdir=rootDir)
print(dirpath)

onlydirs = [os.path.join(dirpath, f) for f in os.listdir(dirpath)
            if os.path.isdir(os.path.join(dirpath, f))]
onlydirs = sorted(onlydirs)

date = []
time = []
localdate = []
localtime = []
exposure = []
gain = []
pitch = []
roll = []
true_heading = []

true_lat = []
true_lon = []

agst = []
dec = []
ra = []
heading = []
lat = []
lon = []

pitch_corr = []
roll_corr = []
proj_angle = []
heading_corr = []
dec_corr = []
ra_corr = []
lat_corr = []
lon_corr = []
nb_bright = []
cpx = []
cpy = []
use_cp = []
use_tc = []
calc_succeeded = []
solve_time = []

for subdir in onlydirs:
    p = Path(subdir)
    corrFiles = [x for x in p.iterdir() if ( x.is_file() and x.name.endswith(".corr") )]
    SUCCEEDED = "TRUE" if len(corrFiles) > 0 else "FALSE"
    calc_succeeded.append(SUCCEEDED)
    print(    "SUCCEEDED:", SUCCEEDED)

    if SUCCEEDED:
        wcsFiles = [x for x in p.iterdir() if ( x.is_file() and x.name.endswith(".wcs") )]
        if len(wcsFiles) > 0:
            wcsFile = wcsFiles[0]
            command = "wcsinfo " + str(wcsFile)
            res = subprocess.getoutput(command).split('\n')[0:2]
            crpix0 = str(round(float(res[0].split(" ")[-1]),2))
            crpix1 = str(round(float(res[1].split(" ")[-1]),2))
            cpx.append(crpix0)
            cpy.append(crpix1)
        else:
            cpx.append("")
            cpy.append("")
    
    resFiles = [x for x in p.iterdir() if ( x.is_file() and x.name == "results.txt" )]
    if len(resFiles) > 0:
        resFile = resFiles[0]
        print(resFile)
        with open(resFile, "r") as f:
            reader = csv.reader(f, delimiter=",")
            year = ""
            month = ""
            day = ""
            hour = ""
            minute = ""
            second = ""
            milliseconds = ""
            localyear = ""
            localmonth = ""
            localday = ""
            localhour = ""
            localminute = ""
            localsecond = ""
            localmillisecond = ""
            dmy = True
            hms = True
            lhms = True
            for i, line in enumerate(reader):
                #print(i, line)
                if line: # if line not empty

                    # true heading from filename
                    if line[0].startswith('input file:'):
                        true_heading.append(line[0].split('/')[-1].split('h')[-1].split('.')[0])

                    # Date (Greenwich)
                    if line[0].startswith('day'):
                        day = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('month'):
                        month = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('year'):
                        year = (line[0].split(': ')[-1])
                    if (day and month and year and dmy):
                        date.append('-'.join([year, month, day]))
                        dmy = False

                    # Time (Greenwith)
                    if line[0].startswith('hour'):
                        hour = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('minute'):
                        minute = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('second'):
                        second = (line[0].split(': ')[-1]).zfill(2)
                    if (hour and minute and second and hms):
                        time.append(':'.join([hour, minute, second]))
                        hms = False

                    # Date (Local)
                    if line[0].startswith('Local Date year'):
                        localyear = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('Local Date month'):
                        localmonth = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('Local Date day'):
                        localday = (line[0].split(': ')[-1]).zfill(2)
                    if (localyear and localmonth and localday):
                        localdate.append('-'.join([localyear, localmonth, localday]))

                    # Time (Local)
                    if line[0].startswith('Local Time hour'):
                        localhour = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('Local Time minute'):
                        localminute = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('Local Time second'):
                        localsecond = (line[0].split(': ')[-1]).zfill(2)
                    if line[0].startswith('Local Time milliSec'):
                        localmillisecond = (line[0].split(': ')[-1]).zfill(3)
                    if (localhour and localminute and localsecond and localmillisecond and lhms):
                        first = ':'.join([localhour, localminute, localsecond])
                        second = '.' + localmillisecond
                        localtime.append(first + second)
                        lhms = False

                    # Exposure
                    if 'exposure' in line[0]:
                        exposure.append(line[0].split(': ')[-1])    
                        
                    # Gain
                    if 'gain' in line[0]:
                        gain.append(line[0].split(': ')[-1])    
                        
                    # Pitch
                    if 'pitch: ' in line[0]:
                        pitch.append(line[0].split(': ')[-1])
                        
                    # Roll
                    if 'roll: ' in line[0]:
                        roll.append(line[0].split(': ')[-1])

                    ## true heading
                    #if 'true heading: ' in line[0]:
                    #    true_heading.append(line[0].split(': ')[-1])

                    true_lat.append(trueLat);
                    true_lon.append(trueLon);

                    # Apparent Greenwich Sidearal Time
                    if 'Apparent Greenwich Sidereal Time:' in line[0]:
                        agst.append(line[0].split(': ')[-1])

                    # Declination
                    if 'declination (deg.)' in line[0]:
                        dec.append(line[0].split(': ')[-1])

                    # Right Ascension
                    if 'right ascension (deg.)' in line[0]:
                        ra.append(line[0].split(': ')[-1])

                    # Rotation Angle
                    if 'rotation angle (deg.)' in line[0]:
                        heading.append(line[0].split(': ')[-1])

                    # Latitude
                    if 'latitude (N)' in line[0]:
                        lat.append(line[0].split(': ')[-1])

                    # Longitude
                    if 'longitude (W)' in line[0]:
                        lon.append(line[0].split(': ')[-1])
                        
                    # corrected Pitch
                    if 'pitch corrected (deg.): ' in line[0]:
                        pitch_corr.append(line[0].split(': ')[-1])
                        
                    # corrected Roll
                    if 'roll corrected (deg.): ' in line[0]:
                        roll_corr.append(line[0].split(': ')[-1])
                        
                    # Projection Angle
                    if 'projection angle: ' in line[0]:
                        proj_angle.append(line[0].split(': ')[-1])


                    # corrected heading
                    if 'heading (deg.)' in line[0]:
                        heading_corr.append(line[0].split(': ')[-1])

                    # corrected declination
                    if 'declination corrected (deg.)' in line[0]:
                        dec_corr.append(line[0].split(': ')[-1])

                    # corrected right ascension
                    if 'right ascension corrected (deg.)' in line[0]:
                        ra_corr.append(line[0].split(': ')[-1])

                    # corrected latitude
                    if 'latitude corrected (N)' in line[0]:
                        lat_corr.append(line[0].split(': ')[-1])

                    # corrected longitude
                    if 'longitude corrected (W)' in line[0]:
                        lon_corr.append(line[0].split(': ')[-1])
                        
                    # Number of Bright Stars
                    if 'number of stars' in line[0]:
                        nb_bright.append(line[0].split(': ')[-1])

                    # Use Center Pixel
                    if 'use center pixel: ' in line[0]:
                        use_cp.append(line[0].split(': ')[-1])

                    # Use tilt correction
                    if 'use tilt correction: ' in line[0]:
                        use_tc.append(line[0].split(': ')[-1])

                    # solve time (millisec)
                    if 'solved in ' in line[0]:
                        solve_time.append(line[0].split(' ')[2])

                        
    else:
        "No Results.txt file exists"

# write results to a CSV file
run_dir_name = dirpath.split('/')[-1] 
outpath = Path(dirpath, run_dir_name+".csv")
with open(outpath, "a") as ap:
    if (os.path.getsize(outpath)) <= 0:
        wr = csv.writer(ap)
        wr.writerow(header)
        for i in range(len(date)):
            wr.writerow( [
                          localdate[i], localtime[i], "", "", "", exposure[i], gain[i],
                          pitch[i], roll[i], true_heading[i], true_lat[i], true_lon[i],
                          agst[i], dec[i], ra[i], heading[i], lat[i], lon[i],
                          pitch_corr[i], roll_corr[i],
                          proj_angle[i], heading_corr[i], dec_corr[i], ra_corr[i],
                          lat_corr[i], lon_corr[i],
                          nb_bright[i], cpx[i], cpy[i], use_cp[i], use_tc[i],
                          calc_succeeded[i], solve_time[i]
                         ] )

        ap.close()

import numpy as np
from tkinter import filedialog
import tkinter as tk
from tkinter import ttk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import warnings
warnings.filterwarnings("ignore")


FIG_SIZE = (5.5,5.5)
BKG_COLOR = 'black'
FACE_COLOR = '#1a1a1a'
THEME_COLOR = 'limegreen'
COLORMAP = 'magma'

##################################################################################################
class MplMap():

    @classmethod
    def settings(cls, root):
        cls.image = np.random.randint(16383, size=(128,128), dtype=np.uint16)
        cls.image.astype('int16')
        plt.style.use('dark_background')
        cls.fig, cls.ax = plt.subplots(figsize=FIG_SIZE, dpi=100)
        cls.fig.patch.set_facecolor(FACE_COLOR)
        cls.ax.spines['left'].set_color(THEME_COLOR)
        cls.ax.spines['right'].set_color(THEME_COLOR)
        cls.ax.spines['top'].set_color(THEME_COLOR)
        cls.ax.spines['bottom'].set_color(THEME_COLOR)
        cls.ax.tick_params(axis='x', colors=THEME_COLOR)
        cls.ax.tick_params(axis='y', colors=THEME_COLOR)
        cls.ax.yaxis.label.set_color(THEME_COLOR)
        cls.ax.xaxis.label.set_color(THEME_COLOR)
        cls.ax.title.set_color(THEME_COLOR)
        cls.fig.tight_layout()
        cls.canvas = FigureCanvasTkAgg(cls.fig, master=root)

    @classmethod
    def get_canvas(cls):
        return cls.canvas

##################################################################################################
class ShowImage(MplMap):

    def __init__(self, master, image):
        self.master = master
        self.image = image
        self.mplmap = MplMap
        self.fig.patch.set_facecolor(FACE_COLOR)
        self.ax.spines['left'].set_color(THEME_COLOR)
        self.ax.spines['right'].set_color(THEME_COLOR)
        self.ax.spines['top'].set_color(THEME_COLOR)
        self.ax.spines['bottom'].set_color(THEME_COLOR)
        self.ax.tick_params(axis='x', colors=THEME_COLOR)
        self.ax.tick_params(axis='y', colors=THEME_COLOR)
        self.ax.yaxis.label.set_color(THEME_COLOR)
        self.ax.xaxis.label.set_color(THEME_COLOR)
        self.ax.set_title('Mean Image')
        self.ax.title.set_color(THEME_COLOR)
        self.fig.tight_layout()
        self.show_image()

    def show_image(self):
        self.image = self.mplmap.image
        self.cax = plt.imshow(self.image, cmap=COLORMAP)
        self.cbar = plt.colorbar(self.cax)
        self.ax.title.set_color(THEME_COLOR) 
        self.fig.tight_layout()
        self.canvas.draw()


##################################################################################################
class Tk_Handler():

    def __init__(self, root, canvas):
        self.root = root
        self.canvas = canvas
        self.root.wm_title("Spectrometer camera image viewer")

        # Bottom button frame
        bottom_frame = tk.Frame(self.root)

        # Quit Button (place away from other buttons)
        quit_button = tk.Button(bottom_frame, text="Quit",\
                                font=("Calibri", 11),\
                                fg='white', bg='darkred',\
                                height=2, width=4, command=self._quit)

        # Place the frames in a grid (row, column)
        tk.Grid.rowconfigure(self.root, 0, weight=1)
        tk.Grid.columnconfigure(self.root, 0, weight=1)
        canvas.get_tk_widget().grid(row=0, column=0, rowspan=1, columnspan=1, pady=2)
        bottom_frame.grid(row=1, column=0, rowspan=1, columnspan=5, pady=0)

        # Place the bottom widgets within a grid
        tk.Grid.rowconfigure(bottom_frame, 0, weight=1)
        tk.Grid.columnconfigure(bottom_frame, 0, weight=1)
        quit_button.grid(row=0, column=0, columnspan=1, padx=10, pady=0)


        self._get_mean_image()
        self.SImg = ShowImage(self.root, self.image)
        tk.mainloop()


    def _quit(self):
        self.root.quit()
        self.root.destroy()

    def _get_mean_image(self):
        imgH, imgW = (128,128)
        infile = filedialog.askopenfilename(initialdir='Data', filetypes=[("raw seq", "*.RAW")])
        data = np.fromfile(infile, dtype=np.uint16)
        nb_frames = int(data.shape[0] / (imgH * imgW))
        print("nb_frames:", nb_frames)
        data = data.reshape((nb_frames,128,128))
        Seq = []
        for iframe in range(data.shape[0]):
            img = data[iframe,:,:]
            #img = np.left_shift(img, 2)
            Seq.append(img)
        Seq = np.array(Seq)
        if Seq.shape[0] > 1:
            meanImg = np.mean(Seq, axis=0)
        else:
            meanImg = Seq
        self.image = meanImg

##################################################################################################
root = tk.Tk()
MplMap.settings(root)
Tk_Handler(root, MplMap.get_canvas())

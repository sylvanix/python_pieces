'''
    Use the Micronix MMC-103 3-Axis Piezo Motor Controller to position the stages.
        * Axis 1 controls the camera focus stage.
        * Axis 2 controls the gratings aperture selection stage.
        * Axis 3 is unused.
     Messages are sent from the calling function to the StageController via the synchronized
        queue "to_micronix_queue". These messages are then passed as commands to the MMC-103
        via serial over USB.
    Messages are sent back from StageController to the calling function via the synchronized
        queue "from_micronix_queue". These mesages inform the calling function of the controller
        status and/or error conditions.
'''

import os, sys
import csv
import time
from pathlib import Path
from threading import Thread
import queue
import serial



''' read_aperture_poitions_file_csv() '''
def read_aperture_positions_file_csv(subdirpath):
    confpath = Path("Config")
    confpath.mkdir(parents=True, exist_ok=True)
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))] 
    positions_file = sorted([x for x in onlyfiles
                             if x.split(".csv")[0].endswith("aperture_positions")])
    positions = []
    names = []
    # read the aperture positions file (if exists)
    if positions_file:
        fn = os.path.join(subdirpath, positions_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    positions.append(float(line[1]))
                    names.append(str(line[2]))
    return positions, names


##################################################################################################
class StageController(Thread):
    def __init__(self, to_micronix_queue, from_micronix_queue, find_center=False):
        # Call Thread constructor
        super().__init__()
        self.keep_running = True
        self.to_micronix_queue = to_micronix_queue # for messages coming from main program
        self.from_micronix_queue = from_micronix_queue # for sending messaged to main program
        self.find_center = find_center
        self.root_dir = Path(__file__).parent.absolute()
        self.config_dir = os.path.join(self.root_dir, 'Config')
        positions, names = read_aperture_positions_file_csv(self.config_dir)
        self.aperture_positions = positions
        self.aperture_names = names
        self.sleepTime = 0.5 # seconds
        self.tiny_step_sz = 0.1
        self.small_step_sz = 0.2
        self.nominal_step_sz = 0.3
        self.big_step_sz = 0.5
        self.stg1_pos_tstr = None
        self.stg1_pos_estr = None
        self.stg2_pos_tstr = None
        self.stg2_pos_estr = None
        self.stage_1_status = "00000000"
        self.stage_2_status = "00000000"
        self.axes = [1,2]
        
        # define the parameters of the serial connection
        self.ser = serial.Serial(
            # you can verify the port from bash with: ~$ ls /dev | grep 'ttyUSB*'
            port='/dev/ttyUSB0',
            baudrate=38400,
            timeout=1,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS
        )
        # check connection
        if self.ser.isOpen():
            print('Connection to MMC-103 established.')
        else:
            print('Unable to connect to MMC-103. Please check hardware.')
            self.stop()
    
        time.sleep(0.1) # need some little time to detect the encoders
        res = self._startup_sequence()

    def stop(self):
        self.ser.close()
        self.keep_running = False

    def run(self):
        # This runs when the calling routine invokes the .start method
        while self.keep_running:
            if self.to_micronix_queue.qsize():
                ret = 1
                msg = self.to_micronix_queue.get()
                if ("MOVE_TO_APERTURE" in msg):
                    ret = self._move_absolute(msg)
                if ("RELATIVE_MOVE" in msg):
                    ret = self._move_relative(msg)
                if ("MOVE_HOME" in msg):
                    ret = self._move_home(msg)
            time.sleep(0.05)

    def _startup_sequence(self):
        # clear any errors
        self._clear_all_error_messages(1)
        self._clear_all_error_messages(2)
        if self.find_center:
            # find center positions of both stages
            self._find_center_position_stage_1()
            self._find_center_position_stage_2()
        # set the velocities to a value lower than 2
        self._set_velocity(1, 1.0)
        self._set_velocity(2, 1.0)
        # inform the calling function that stages are ready
        self.from_micronix_queue.put("STAGE 1 OK")
        self.from_micronix_queue.put("STAGE 2 OK")
        # change to closed loop mode
        self._set_feedback_mode(1, 2) 
        self._set_feedback_mode(2, 2) 
        # get current positions
        time.sleep(self.sleepTime)
        self._get_axis_position(1, False)
        self._get_axis_position(2, False) 
        return 0

    def _find_center_position_stage_1(self):
        stage_nb = 1
        # assumes the default velocity is 2.00 (default)
        self._set_feedback_mode(stage_nb, 0)
        # move to the negative limit
        self._move_to_negative_limit(stage_nb)
        # check that error register bits are cleared
        cmd = "STAGE" + str(stage_nb) + "_MNL"
        time.sleep(0.1)
        res = self._inform_when_stage_stopped(cmd)
        # set as temporary zero position
        time.sleep(self.sleepTime)
        self._set_zero_position(stage_nb)
        # move relative half full travel in the positive direction
        pos = 5.5
        serial_cmd = str(stage_nb) + "MVR" + str(pos)
        ret = self.write_serial(serial_cmd)
        self.read_serial()
        time.sleep(0.1)
        # check that error register bits are cleared
        res = self._inform_when_stage_stopped(cmd)
        time.sleep(0.1)
        self._get_axis_position(stage_nb, False) 
        # set this position as the new zero
        self._set_zero_position(stage_nb)
        # inform calling function that move is complete
        time.sleep(self.sleepTime)
        self._get_axis_position(stage_nb, False) 
        self.from_micronix_queue.put("STAGE " + str(stage_nb) + " OK")
        return 0
        

    def _find_center_position_stage_2(self):
        stage_nb = 2
        # assumes the default velocity is 2.00 (default)
        self._set_feedback_mode(stage_nb, 0)
        # move to the negative limit
        self._move_to_negative_limit(stage_nb)
        # check that error register bits are cleared
        cmd = "STAGE" + str(stage_nb) + "_MNL"
        time.sleep(0.1)
        res = self._inform_when_stage_stopped(cmd)
        # set as temporary zero position
        self._set_zero_position(stage_nb)
        time.sleep(0.1)
        self._get_axis_position(stage_nb, True)
        # now move in to the positive limit
        self._move_to_positive_limit(stage_nb)
        # check that error register bits are cleared
        cmd = "STAGE" + str(stage_nb) + "_MPL"
        time.sleep(self.sleepTime)
        res = self._inform_when_stage_stopped(cmd)
        self._get_axis_position(stage_nb, False)
        dist = self.stg2_pos_encoder
        dist = float(dist)/2
        # change to closed loop mode
        self._set_feedback_mode(stage_nb, 2) 
        # send the MVA command to go to center position
        serial_cmd = str(stage_nb) + "MVA" + str(dist)
        ret = self.write_serial(serial_cmd)
        self.read_serial()
        time.sleep(0.1)
        # check that error register bits are cleared
        res = self._inform_when_stage_stopped(cmd)
        time.sleep(0.1)
        self._get_axis_position(stage_nb, False) 
        # set this position as the new zero
        self._set_zero_position(stage_nb)
        # inform calling function that move is complete
        time.sleep(self.sleepTime)
        self._get_axis_position(stage_nb, False) 
        self.from_micronix_queue.put("STAGE " + str(stage_nb) + " OK")
        return 0

    def _move_home(self, cmd):
        stage = 1 if "STAGE1" in cmd else 2
        pos = 0.0
        serial_cmd = str(stage) + "MVA" + str(pos)
        ret = self.write_serial(serial_cmd)
        self.read_serial()
        res = self._inform_when_stage_stopped(cmd)
        time.sleep(self.sleepTime)
        ret = self._get_axis_position(stage, False)
        # inform calling function that move is complete
        self.from_micronix_queue.put("STAGE " + str(stage) + " OK")
        return res 
 
    def _move_absolute(self, cmd):
        stage = 1 if "STAGE1" in cmd else 2
        pnumb = int(cmd.split("_")[-1]) - 1 # indexs start at 0
        pos = self.aperture_positions[pnumb]
        serial_cmd = str(stage) + "MVA" + str(pos)
        ret = self.write_serial(serial_cmd)
        self.read_serial()
        res = self._inform_when_stage_stopped(cmd)
        time.sleep(self.sleepTime)
        ret = self._get_axis_position(stage, False)
        # inform calling function that move is complete
        self.from_micronix_queue.put("STAGE " + str(stage) + " OK")
        return res

    def _inform_when_stage_stopped(self, cmd, timeout_secs=20):
        stage = 1 if "STAGE1" in cmd else 2
        # keep checking axis status before issuing command
        keep_checking = True
        start_time = time.time()
        elapsed_time = 0.0
        error_sig = 0
        while keep_checking:
            status_bits = self._get_axis_status(stage, False)
            # if bit 3 is high, end this while loop
            # ( bit 3 is status_bits[4] )
            if ( (status_bits[4]==1) or (1 not in status_bits) ):
                keep_checking = False
            current_time = time.time()
            elapsed_time = current_time - start_time
            # if errors are still present after timeout_secs, then end this while loop
            if elapsed_time > timeout_secs:
                self.from_micronix_queue.put("errors(s) preventing " + cmd)
                error_sig = 1
                keep_checking = False
            time.sleep(0.1) # keep the while loop from checking too often
        if not error_sig:
            return 0
        else:
            return 1

    def _get_axis_status(self, axis_nb, verbose=False):
        self.write_serial(str(axis_nb) + "STA?")
        res = self.read_serial()
        # strip # from res string and convert to int
        res = int(res[1:]) # cut off the leading "#" and convert to int
        bstr = bin(res)[2:].zfill(8) # convert to binary, remove "0b", and zero-fill
        status_strings = self._check_status_register_bits(bstr)
        return [int(x) for x in bstr]

    def _check_status_register_bits(self, statusStr): 
        # decode the status register string
        # see the STA section (5-62) of the MMC-103 Reference Manual
        outlist = []
        b7,b6,b5,b4,b3,b2,b1,b0 = [x for x in statusStr]
        if int(b7): outlist.append(" *** One of more errors have occured") # use ERR? or CER to clear
        if int(b6): outlist.append(" *** Currently in Acceleration phase of motion") 
        if int(b5): outlist.append(" *** Currently in Constant Velocity phase of motion")
        if int(b4): outlist.append(" *** Currently in Deceleration phase of motion")
        if int(b3): outlist.append(" *** Stage is moving")
        if int(b2): outlist.append(" *** A Program is currently running")
        if int(b1): outlist.append(" *** Positive Switch is Activated")
        if int(b0): outlist.append(" *** Negative Switch is Activated")
        return outlist

    def _move_relative(self, cmd):
        # process the args
        stage = 1 if "STAGE1" in cmd else 2
        sign = "" if "POSITIVE" in cmd else "-"
        if "TINY" in cmd:
            sz = self.tiny_step_sz
        elif "SMALL" in cmd:
            sz = self.small_step_sz
        elif "NOMINAL" in cmd:
            sz = self.nominal_step_sz
        elif "BIG" in cmd:
            sz = self.big_step_sz
        else:
            sz = self.tiny_step_sz
        # send the MVR command
        serial_cmd = str(stage) + "MVR" + sign + str(sz)
        ret = self.write_serial(serial_cmd)
        self.read_serial()
        res = self._inform_when_stage_stopped(cmd)
        time.sleep(self.sleepTime)
        ret = self._get_axis_position(stage, False)
        # inform calling function that move is complete
        self.from_micronix_queue.put("STAGE " + str(stage) + " OK")
        return res

    def _get_axis_position(self, axis_nb, verbose=False):
        self.write_serial(str(axis_nb) + "POS?")
        pos = self.read_serial()
        theo, enco = pos[1:].split(',')
        print("*** theo:", theo, " enco:", enco)
        if axis_nb == 1:
            self.stg1_pos_theoretical = float(theo)
            self.stg1_pos_encoder = float(enco)
            self.stg1_pos_tstr = theo
            self.stg1_pos_estr = enco
            if verbose:
                print("stage 1 encoder position:", self.stg1_pos_encoder)
        elif axis_nb == 2:
            self.stg2_pos_theoretical = float(theo)
            self.stg2_pos_encoder = float(enco)
            self.stg2_pos_tstr = theo
            self.stg2_pos_estr = enco
            if verbose:
                print("stage 2 encoder position:", self.stg2_pos_encoder)
        return 0

    def _clear_all_error_messages(self, axis_nb):
        self.write_serial(str(axis_nb) + "CER")
        res = self.read_serial()

    def _set_zero_position(self, axis_nb): # sets current position as zero
        self.write_serial(str(axis_nb) + "ZRO")
        res = self.read_serial()

    def _save_startup_position(self, axis_nb): # sets the startup position as the current position
        self.write_serial(str(axis_nb) + "SVP")
        res = self.read_serial()

    def _set_feedback_mode(self, axis_nb, mode):
        self.write_serial(str(axis_nb) + "FBK" + str(mode))
        self.read_serial()

    def _get_feedback_mode(self, axis_nb, verbose=False):
        self.write_serial(str(axis_nb) + "FBK?")
        mode = self.read_serial()
        #mode = mode.split("#")[-1]
        if verbose:
            print("stage " + str(axis_nb) + " feedback mode:", mode)

    def _set_velocity(self, axis_nb, vel):
        self.write_serial(str(axis_nb) + "VEL" + str(vel))
        self.read_serial()

    def _get_velocity(self, axis_nb, verbose=False):
        self.write_serial(str(axis_nb) + "VEL?")
        vel = self.read_serial()
        vel = vel.split("#")[-1]
        if verbose:
            print("stage " + str(axis_nb) + " velocity:", vel)

    def _move_to_negative_limit(self, axis_nb):
        self.write_serial(str(axis_nb) + "MLN")
        self.read_serial()

    def _move_to_positive_limit(self, axis_nb):
        self.write_serial(str(axis_nb) + "MLP")
        self.read_serial()

    def _check_for_errors(self, axis_nb, verbose=False):
        self.write_serial(str(axis_nb) + "ERR?")
        res = self.read_serial()
        if len(res) > 2:
            self.from_micronix_queue.put("STAGE " + str(axis_nb) + " ERROR")
            if verbose:
                print( "stage " + str(axis_nb) + " error: " + res )

    def write_serial(self, qstring):
        qstring = qstring + "\n\r"
        cmdstr = qstring.encode(encoding="UTF-8")
        self.ser.write(cmdstr)
        #print('\nCOMMAND :', qstring[:-2])
    
    def read_serial(self):
        time.sleep(self.sleepTime)
        out = b''
        while self.ser.inWaiting() > 0:
            out += self.ser.read()
        outStr = out.decode('utf-8')
        if outStr[-2:]=='\n\r': outStr=outStr[:-2]
        elif outStr[-1:]=='\n': outStr=outStr[:-1]
        #print('RESPONSE:', outStr)
        return outStr



##################################################################################################
class VirtualStageController(Thread):
    def __init__(self, to_micronix_queue, from_micronix_queue, find_center=False):
        # Call Thread constructor
        super().__init__()
        self.keep_running = True
        self.to_micronix_queue = to_micronix_queue # for messages coming from main program
        self.from_micronix_queue = from_micronix_queue # for sending messaged to main program
        self.find_center = find_center
        self.root_dir = Path(__file__).parent.absolute()
        self.config_dir = os.path.join(self.root_dir, 'Config')
        positions, names = read_aperture_positions_file_csv(self.config_dir)
        self.aperture_positions = positions
        self.aperture_names = names
        self.sleepTime = 0.5 # seconds
        self.tiny_step_sz = 0.1
        self.small_step_sz = 0.2
        self.nominal_step_sz = 0.3
        self.big_step_sz = 0.5
        self.stg1_pos_tstr = None
        self.stg1_pos_estr = None
        self.stg2_pos_tstr = None
        self.stg2_pos_estr = None
        self.stage_1_status = "00000000"
        self.stage_2_status = "00000000"
        self.axes = [1,2]
        # tell the main function that stages are ready
        self.from_micronix_queue.put("STAGE 1 OK")
        self.from_micronix_queue.put("STAGE 2 OK")

    def stop(self):
        self.keep_running = False

    def run(self):
        # This runs when the calling routine invokes the .start method
        while self.keep_running:
            if self.to_micronix_queue.qsize():
                ret = 1
                msg = self.to_micronix_queue.get()
                if ("MOVE_TO_APERTURE" in msg):
                    ret = self._move_absolute(msg)
                if ("RELATIVE_MOVE" in msg):
                    ret = self._move_relative(msg)
                if ("MOVE_HOME" in msg):
                    ret = self._move_home(msg)
            time.sleep(0.05)

    def _move_absolute(self, msg):
        stage = 2
        time.sleep(0.5)
        # inform calling function that move is complete
        self.from_micronix_queue.put("STAGE " + str(stage) + " OK")
        return 0

    def _move_relative(self, cmd):
        # process the args
        stage = 1 if "STAGE1" in cmd else 2
        time.sleep(0.5)
        # inform calling function that move is complete
        self.from_micronix_queue.put("STAGE " + str(stage) + " OK")
        return 0

    def _move_home(self, cmd):
        stage = 1 if "STAGE1" in cmd else 2
        time.sleep(0.5)
        # inform calling function that move is complete
        self.from_micronix_queue.put("STAGE " + str(stage) + " OK")
        return 0 



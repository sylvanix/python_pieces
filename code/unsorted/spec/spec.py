import os, time, csv
import socket
import queue
from threading import Thread
import select
from pathlib import Path
import argparse
import numpy as np
import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext
from tkinter import filedialog
from tkinter import messagebox
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import warnings
warnings.filterwarnings("ignore")

import stage_controller
import camera


FIG_SIZE = (5.5,5.5)
FIG2_SIZE = (5.5,4.5)
BKG_COLOR = 'black'
FACE_COLOR = '#1a1a1a'
THEME_COLOR = 'limegreen'
LINE_COLOR = '#00b3b3'
BAR_COLOR = '#cc00cc'
LOGGING_COLOR = 'limegreen'
COLORMAP = 'magma'
FONTNAME = ''
ENDL = "\n"


''' write_spectral_data() '''
def write_spectral_data(xs, ys, prefix, dset, stage_pos, meas_type):
    timestr = time.strftime("%Y%m%d-%H%M%S")
    subdirpath = Path(os.path.join('Data', prefix))
    subdirpath.mkdir(parents=True, exist_ok=True)
    subdirpath = Path(os.path.join(subdirpath, 'Spectral'))
    subdirpath.mkdir(parents=True, exist_ok=True)
    subdirpath = Path(os.path.join(subdirpath, meas_type))
    subdirpath.mkdir(parents=True, exist_ok=True)
    
    filename = os.path.join(subdirpath, dset+'_'+timestr+'_'+str(stage_pos).zfill(2)+'.csv')
    header = "Wavelength, Intensity\n"
    with open(filename, "a") as ap:
        if (os.path.getsize(filename)) <= 0:
            ap.write(header)
            for i in range(len(xs)):
                ap.write( str(xs[i]) + ", " + str(ys[i]) +'\n' )
            ap.close()


''' write_gratings_exposures() '''
def write_exposures(expList):
    # save the current file as prev then write new file
    subdirpath = Path("Config")
    subdirpath.mkdir(parents=True, exist_ok=True)
    filename = os.path.join(subdirpath, 'gratings_exposures.csv')

    # check if file already exists
    if os.path.exists(filename):
        newname = filename.split(".csv")[0] + "_prev.csv"
        if not os.path.exists(newname):
            os.rename(filename, newname)
        else:
            os.replace(filename, newname)

    header = "grating exposure times in microseconds\n"
    with open(filename, "a") as ap:
        if (os.path.getsize(filename)) <= 0:
            ap.write(header)
            for i in range(len(expList)):
                thisVal = str(expList[i])
                ap.write( thisVal + '\n' )
            ap.close()


''' write_gratings_selected() '''
def write_gratings_selected(sList):
    # save the current file as prev then write new file
    subdirpath = Path("Config")
    subdirpath.mkdir(parents=True, exist_ok=True)
    filename = os.path.join(subdirpath, 'gratings_selected.csv')

    # check if file already exists
    if os.path.exists(filename):
        newname = filename.split(".csv")[0] + "_prev.csv"
        if not os.path.exists(newname):
            os.rename(filename, newname)
        else:
            os.replace(filename, newname)

    header = 'gratings selected\n'
    with open(filename, "a") as ap:
        if (os.path.getsize(filename)) <= 0:
            ap.write(header)
            for i in range(len(sList)):
                thisVal = str(sList[i])
                ap.write( thisVal +'\n' )
            ap.close()


''' write_frames() '''
def write_frames(fList):
    subdirpath = Path("Config")
    subdirpath.mkdir(parents=True, exist_ok=True)
    filename = os.path.join(subdirpath, 'frames.csv')
    # check if file already exists
    if os.path.exists(filename):
        newname = filename
        if os.path.exists(newname):
            os.replace(filename, newname)
    header = 'number of frames to write\n'
    with open(filename, "w") as ap:
        #if os.path.exists(filename):
        ap.write(header)
        for i in range(len(fList)):
            thisVal = str(fList[i])
            ap.write( thisVal + '\n' )
        ap.close()


''' get_selected_csv() '''
def get_selected_csv(subdirpath, set_name):
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))]
    onlyfiles = [x for x in onlyfiles if x.split("_")[0] == set_name]
    pos_1_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("01")])
    pos_2_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("02")])
    pos_3_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("03")])
    pos_4_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("04")])
    pos_5_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("05")])
    pos_6_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("06")])
    pos_7_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("07")])
    pos_8_files = sorted([x for x in onlyfiles if x.split(".csv")[0].endswith("08")])
    
    pos_files = []
    if pos_1_files:
        pos_files.append(pos_1_files[-1])
    if pos_2_files:
        pos_files.append(pos_2_files[-1])
    if pos_3_files:
        pos_files.append(pos_3_files[-1])
    if pos_4_files:
        pos_files.append(pos_4_files[-1])
    if pos_5_files:
        pos_files.append(pos_5_files[-1])
    if pos_6_files:
        pos_files.append(pos_6_files[-1])
    if pos_7_files:
        pos_files.append(pos_7_files[-1])
    if pos_8_files:
        pos_files.append(pos_8_files[-1])
    
    # read the files
    ys = []
    for mrpfile in pos_files:
        fn = os.path.join(subdirpath, mrpfile)

        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            yc = []
            xc = []
            for i, line in enumerate(reader):
                if i>0:
                    xc.append(int(line[0]))
                    yc.append(float(line[1])) 
        ys += yc
    xs = [x for x in range(len(ys))]

    return ys, xs, len(pos_files)


''' read_config_files_csv() '''
def read_config_files_csv(subdirpath):
    confpath = Path("Config")
    confpath.mkdir(parents=True, exist_ok=True)
    onlyfiles = [f for f in os.listdir(subdirpath)
                 if os.path.isfile(os.path.join(subdirpath, f))] 
    exposures_file = sorted([x for x in onlyfiles
                             if x.split(".csv")[0].endswith("exposures")])
    exposures_prev_file = sorted([x for x in onlyfiles
                                  if x.split(".csv")[0].endswith("exposures_prev")])
    selected_file = sorted([x for x in onlyfiles
                            if x.split(".csv")[0].endswith("selected")])
    selected_prev_file = sorted([x for x in onlyfiles
                                 if x.split(".csv")[0].endswith("selected_prev")])
    frames_file = sorted([x for x in onlyfiles
                          if x.split(".csv")[0].endswith("frames")])
    exps = []
    exps_prev = []
    sel = []
    sel_prev = []
    frames = []
    # read the exposures file (if exists)
    if exposures_file:
        fn = os.path.join(subdirpath, exposures_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    exps.append(int(line[0]))

    # read the exposures_prev file (if exists)
    if exposures_prev_file:
        fn = os.path.join(subdirpath, exposures_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    exps_prev.append(int(line[0]))

    # read the exposures file (if exists)
    if selected_file:
        fn = os.path.join(subdirpath, selected_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    sel.append(int(line[0]))

    # read the exposures_prev file (if exists)
    if selected_prev_file:
        fn = os.path.join(subdirpath, selected_prev_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    sel_prev.append(int(line[0]))

    # read the frames file (if exists)
    nb_frames = 128
    if frames_file:
        fn = os.path.join(subdirpath, frames_file[0])
        with open(fn, "r") as f:
            reader = csv.reader(f, delimiter=",")
            for i, line in enumerate(reader):
                if i>0 and line:
                    frames.append(int(line[0]))
            nb_frames = frames[0]

    return (exps, exps_prev, sel, sel_prev, nb_frames)

def get_current_time():
    return time.strftime('%y/%m/%d %H:%M:%S')


def get_YMD():
    return time.strftime('_%y_%m_%d')


def get_HMS():
    return time.strftime('%H:%M:%S')

##################################################################################################

''' Logfile '''
class Logfile():
    def __init__(self, prefix, meas_type):
        self.ymd = get_YMD()
        self.prefix = prefix
        self.meas_type = meas_type
        self.subdirpath = Path(os.path.join('Data', prefix))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.subdirpath = Path(os.path.join(self.subdirpath, 'Spectral'))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.subdirpath = Path(os.path.join(self.subdirpath, meas_type))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        
        self.filename = os.path.join(self.subdirpath, 'logfile' + self.ymd + '.txt')
        self.lf = open(self.filename, "w")
        if (os.path.getsize(self.filename)) <= 0:
            self.lf.write( get_HMS() + " - set measurement type: " + meas_type + ENDL )
            self.close_logfile()
    
    def append(self, logtext):
        ymd = get_YMD()
        if (ymd != self.ymd):
            self.ymd = ymd
            self.filename = os.path.join(self.subdirpath, 'logfile' + self.ymd + '.txt')
            self.lf = open(self.filename, "w")
            self.lf.write(logtext + ENDL);
            self.close_logfile()
        self.open_logfile()
        self.lf.write(logtext + ENDL);
        self.close_logfile()

    def open_logfile(self):
        self.lf = open(self.filename, "a")

    def close_logfile(self):
        self.lf.close()


''' Templogfile '''
class Templogfile():
    def __init__(self, prefix, meas_type):
        self.ymd = get_YMD()
        self.subdirpath = Path(os.path.join('Data', prefix))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.subdirpath = Path(os.path.join(self.subdirpath, 'Spectral'))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        self.subdirpath = Path(os.path.join(self.subdirpath, meas_type))
        self.subdirpath.mkdir(parents=True, exist_ok=True)
        
        self.filename = os.path.join(self.subdirpath, 'temperature_log' + self.ymd + '.txt')
        self.lf = open(self.filename, "w")
        csvwriter = csv.writer(self.lf, delimiter=',')
        csvwriter.writerow(["time","t_1","t_2","t_3","t_4"])
        self.close_logfile()
    
    def append(self, logtext):
        ymd = get_YMD()
        if (ymd != self.ymd):
            self.ymd = ymd
            self.filename = os.path.join(self.subdirpath, 'temperature_log' + self.ymd + '.txt')
            self.lf = open(self.filename, "w")
        hms = get_HMS()
        self.open_logfile()
        csvwriter = csv.writer(self.lf, delimiter=',')
        logtemps = logtext.split(",")
        logtemps = [x.split(": ")[-1] for x in logtemps]
        csvwriter.writerow([hms] + logtemps)
        self.close_logfile()

    def open_logfile(self):
        self.lf = open(self.filename, "a")

    def close_logfile(self):
        self.lf.close()

##################################################################################################

''' Reciver: listens for UDP messages from cryo-controller '''
class Receiver(Thread):
    def __init__(self, sock, queue):
        # Call Thread constructor
        super().__init__()
        self.sock = sock
        self.keep_running = True
        self.queue = queue

    def stop(self):
        # Call this from another thread to stop the receiver
        self.keep_running = False

    def run(self):
        # This will run when you call .start method
        while self.keep_running:
            # Use select here so that we don't hang forever in recvfrom.
            # Wake up every .5 seconds to check whether we should keep running.
            rfds, _wfds, _xfds = select.select([self.sock], [], [], 0.5)
            if self.sock in rfds:
                try:
                    data, addr = self.sock.recvfrom(1024)
                    #print("received message:", data.decode('utf-8'))
                    #print("from:", addr)
                    self.queue.put(data)
                    
                except socket.error as err:
                    print("Error from receiving socket {}".format(err))

##################################################################################################

class MplMap():

    @classmethod
    def settings(cls, root):
        cls.root_dir = Path(__file__).parent.absolute()
        cls.imgpath = os.path.join(cls.root_dir, "SINGLE_FRAME.RAW")
        cls.image = np.random.randint(16383, size=(128,128), dtype=np.uint16)
        cls.image.astype('int16').tofile(cls.imgpath)
        cls.x1 = [0]
        cls.x2 = [0]
        cls.x3 = [0]
        cls.y1 = [0]
        cls.y2 = [0]
        cls.y3 = [0]
        cls.x = [0]
        cls.y = [0]
        plt.style.use('dark_background')
        cls.fig, cls.ax = plt.subplots(figsize=FIG_SIZE, dpi=100)
        cls.fig.patch.set_facecolor(FACE_COLOR)
        cls.ax.spines['left'].set_color(THEME_COLOR)
        cls.ax.spines['right'].set_color(THEME_COLOR)
        cls.ax.spines['top'].set_color(THEME_COLOR)
        cls.ax.spines['bottom'].set_color(THEME_COLOR)
        cls.ax.tick_params(axis='x', colors=THEME_COLOR)
        cls.ax.tick_params(axis='y', colors=THEME_COLOR)
        cls.ax.yaxis.label.set_color(THEME_COLOR)
        cls.ax.xaxis.label.set_color(THEME_COLOR)
        cls.ax.title.set_color(THEME_COLOR)
        cls.fig.tight_layout()
        cls.canvas = FigureCanvasTkAgg(cls.fig, master=root)
        cls.fig2, cls.ax2 = plt.subplots(figsize=FIG2_SIZE, dpi=100)
        cls.fig2.tight_layout()
        cls.cax = plt.imshow(cls.image)
        cls.fig2.tight_layout()
        cls.canvas2 = FigureCanvasTkAgg(cls.fig2, master=root)

    @classmethod
    def get_single_frame(cls):
        img = np.fromfile(cls.imgpath, dtype=np.uint16)
        if img.size == 128*128:
            img = img.reshape((128,128))
        else:
            img = np.zeros((128,128), dtype=np.uint16)
        return img

    @classmethod
    def get_canvas(cls):
        return cls.canvas

    @classmethod
    def get_canvas2(cls):
        return cls.canvas2


class PlotLine(MplMap):

    def __init__(self):
        self._lw = 2
        self.mplmap = MplMap

    def draw_plot(self, plot_title, nb_lines):
        self.nb_lines = nb_lines
        self.clear_plot()
        self.ax.plot(self.x, self.y, lw=self._lw, color=LINE_COLOR)
        self.add_slit_range_lines()
        self.ax.set_xlabel('wavelength (' + u'\u03bc' + 'm)')
        self.ax.set_ylabel('intensity (counts)')
        self.ax.set_title(plot_title)
        self.ax.title.set_color(THEME_COLOR)
        self.fig.tight_layout()
        self.canvas.draw()

    def add_slit_range_lines(self):
        # show slit range lines (dividing the regions of the combined curve)
        self.ax.axvline(0, color='#1a1a1a')
        if (self.nb_lines==1):
            self.ax.axvline(0, color='#1a1a1a')    
        else:
            for ii in range(1, self.nb_lines):
                self.ax.axvline((len(self.x)//self.nb_lines)*ii, color='#1a1a1a')
        self.ax.axvline(len(self.x), color='#1a1a1a')

    def clear_plot(self):
        self.ax.clear()


class ShowImage(MplMap):

    def __init__(self, master, has_cb, image):
        self.master = master
        self.image = image
        self.mplmap = MplMap
        self.has_colorbar = has_cb
        self.fig2.patch.set_facecolor(FACE_COLOR)
        self.ax2.spines['left'].set_color(THEME_COLOR)
        self.ax2.spines['right'].set_color(THEME_COLOR)
        self.ax2.spines['top'].set_color(THEME_COLOR)
        self.ax2.spines['bottom'].set_color(THEME_COLOR)
        self.ax2.tick_params(axis='x', colors=THEME_COLOR)
        self.ax2.tick_params(axis='y', colors=THEME_COLOR)
        self.ax2.yaxis.label.set_color(THEME_COLOR)
        self.ax2.xaxis.label.set_color(THEME_COLOR)
        self.ax2.set_title('Live Image')
        self.ax2.title.set_color(THEME_COLOR) 
        self.fig2.tight_layout()
        self.show_image()

    def clear_image(self):
        if self.has_colorbar:
            self.cbar.remove()
            self.has_colorbar = False
            self.ax2.cla()

    def show_image(self):
        self.clear_image()
        self.image = self.mplmap.get_single_frame()
        self.cax = plt.imshow(self.image, cmap=COLORMAP)
        self.cbar = plt.colorbar(self.cax)
        self.has_colorbar = True
        self.ax2.set_title('Live Image')
        self.ax2.title.set_color(THEME_COLOR) 
        self.fig2.tight_layout()
        self.canvas2.draw()
        self.job = self.master.after(100, self.show_image)

    def cancel_self(self):
        self.master.after_cancel(self.job)
        self.cbar.remove()
        self.cax.remove()

    
class ShowCroppedImage(MplMap):
    def __init__(self, master, image):
        self.master = master
        self.image = image
        self.mplmap = MplMap
        self.ax2.set_anchor((0.3, 0.9))
        # [x, y, w, h]
        self.ax3 = self.fig2.add_axes([0.1,0.1,0.85,0.5])
        self.ax3.spines['left'].set_color(THEME_COLOR)
        self.ax3.spines['right'].set_color(THEME_COLOR)
        self.ax3.spines['top'].set_color(THEME_COLOR)
        self.ax3.spines['bottom'].set_color(THEME_COLOR)
        self.ax3.tick_params(axis='x', colors=THEME_COLOR)
        self.ax3.tick_params(axis='y', colors=THEME_COLOR)
        self.ax3.yaxis.label.set_color(THEME_COLOR)
        self.ax3.xaxis.label.set_color(THEME_COLOR)
        self.fig2.tight_layout()
        self.show_image()

    def clear_image(self):
        self.ax2.cla()
        self.ax3.cla()

    def show_image(self):
        self.clear_image()
        self.image = self.mplmap.get_single_frame()
        # crop image around the receiving row
        irow = 64 # IROW_SIGNAL
        halfw = 10
        self.cropped_image = self.image[irow-halfw:irow+halfw+1, :]
        y = self.image[irow, :]
        x = np.array([x for x in range(y.size)])
        self.cax = self.ax2.imshow(self.cropped_image, cmap=COLORMAP)
        self.ax3.bar(x, y, color=BAR_COLOR)
        ylabels = [x for x in range(irow-halfw, irow+halfw+1)]
        self.ax2.set_yticks([0, halfw, 2*halfw])
        self.ax2.set_yticklabels([str(ylabels[0]),str(irow), str(ylabels[-1])], minor=False)
        self.ax2.set_title('Live Image (Region of Signal)')
        self.ax2.title.set_color(THEME_COLOR) 
        self.fig2.tight_layout()
        self.canvas2.draw()
        self.job = self.master.after(100, self.show_image)
      
    def cancel_self(self):
        self.master.after_cancel(self.job)
        self.ax3.remove()


class ConfigurationNotebook:
    def __init__(self, to_micronix_queue):
        self.window = tk.Toplevel()
        self.to_micronix_queue = to_micronix_queue
        self.default_exp = 100
        self.texp_1 = self.default_exp
        self.texp_2 = self.default_exp

        label = tk.Label(self.window, text="This is a test.")
        n = ttk.Notebook(self.window)
        f1 = ttk.Frame(n, height=390, width=250) # first page
        f2 = ttk.Frame(n, height=390, width=250) # second page
        f3 = ttk.Frame(n, height=390, width=250) # third page
        f4 = ttk.Frame(n, height=390, width=250) # fourth page
        n.add(f1, text='Exposure')
        n.add(f2, text='Gratings')
        n.add(f3, text='Focus')
        n.add(f4, text='Frames')

        # read the config files
        exps, exps_prev, sel, sel_prev, nframes = read_config_files_csv("Config")

        ''' f1 - Exposure '''
        spacer0 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp1 = tk.Label(f1, text='Grating 1:')
        self.texp1 = tk.IntVar()
        texp_entry1 = ttk.Entry(f1, width=9, textvariable=self.texp1)
        label_units1 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer1 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp2 = tk.Label(f1, text='Polystyrene 1:')
        self.texp2 = tk.IntVar()
        texp_entry2 = ttk.Entry(f1, width=9, textvariable=self.texp2)
        label_units2 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer2 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp3 = tk.Label(f1, text='Blank 1:')
        self.texp3 = tk.IntVar()
        texp_entry3 = ttk.Entry(f1, width=9, textvariable=self.texp3)
        label_units3 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer3 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp4 = tk.Label(f1, text='Grating 2:')
        self.texp4 = tk.IntVar()
        texp_entry4 = ttk.Entry(f1, width=9, textvariable=self.texp4)
        label_units4 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer4 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp5 = tk.Label(f1, text='Polystyrene 2:')
        self.texp5 = tk.IntVar()
        texp_entry5 = ttk.Entry(f1, width=9, textvariable=self.texp5)
        label_units5 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer5 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp6 = tk.Label(f1, text='Blank 2:')
        self.texp6 = tk.IntVar()
        texp_entry6 = ttk.Entry(f1, width=9, textvariable=self.texp6)
        label_units6 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer6 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp7 = tk.Label(f1, text='Grating 3:')
        self.texp7 = tk.IntVar()
        texp_entry7 = ttk.Entry(f1, width=9, textvariable=self.texp7)
        label_units7 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer7 = tk.Label(f1, height=1, text='') # VERTICAL SPACER
        label_exp8 = tk.Label(f1, text='Polystyrene 3:')
        self.texp8 = tk.IntVar()
        texp_entry8 = ttk.Entry(f1, width=9, textvariable=self.texp8)
        label_units8 = tk.Label(f1, text=u'\u03bc'+'Sec.')
        spacer8 = tk.Label(f1, height=1, text='') # VERTICAL SPACER

        # set the default exposures to values found in current exposures file
        if exps:
            self.texp1.set(exps[0])
            self.texp2.set(exps[1])
            self.texp3.set(exps[2])
            self.texp4.set(exps[3])
            self.texp5.set(exps[4])
            self.texp6.set(exps[5])
            self.texp7.set(exps[6])
            self.texp8.set(exps[7])
            
        spacer0.grid(row=0, column=0, columnspan=3, sticky='nswe')
        label_exp1.grid(row=1, column=0, columnspan=1, sticky='nswe')
        texp_entry1.grid(row=1, column=1, columnspan=1, sticky='nswe')
        label_units1.grid(row=1, column=2, columnspan=1, sticky='nswe')
        spacer1.grid(row=2, column=0, columnspan=3, sticky='nswe')
        label_exp2.grid(row=3, column=0, columnspan=1, sticky='nswe')
        texp_entry2.grid(row=3, column=1, columnspan=1, sticky='nswe')
        label_units2.grid(row=3, column=2, columnspan=1, sticky='nswe')
        spacer2.grid(row=4, column=0, columnspan=3, sticky='nswe')
        label_exp3.grid(row=5, column=0, columnspan=1, sticky='nswe')
        texp_entry3.grid(row=5, column=1, columnspan=1, sticky='nswe')
        label_units3.grid(row=5, column=2, columnspan=1, sticky='nswe')
        spacer3.grid(row=6, column=0, columnspan=3, sticky='nswe')
        label_exp4.grid(row=7, column=0, columnspan=1, sticky='nswe')
        texp_entry4.grid(row=7, column=1, columnspan=1, sticky='nswe')
        label_units4.grid(row=7, column=2, columnspan=1, sticky='nswe')
        spacer4.grid(row=8, column=0, columnspan=3, sticky='nswe')
        label_exp5.grid(row=9, column=0, columnspan=1, sticky='nswe')
        texp_entry5.grid(row=9, column=1, columnspan=1, sticky='nswe')
        label_units5.grid(row=9, column=2, columnspan=1, sticky='nswe')
        spacer5.grid(row=10, column=0, columnspan=3, sticky='nswe')
        label_exp6.grid(row=11, column=0, columnspan=1, sticky='nswe')
        texp_entry6.grid(row=11, column=1, columnspan=1, sticky='nswe')
        label_units6.grid(row=11, column=2, columnspan=1, sticky='nswe')
        spacer6.grid(row=12, column=0, columnspan=3, sticky='nswe')
        label_exp7.grid(row=13, column=0, columnspan=1, sticky='nswe')
        texp_entry7.grid(row=13, column=1, columnspan=1, sticky='nswe')
        label_units7.grid(row=13, column=2, columnspan=1, sticky='nswe')
        spacer7.grid(row=14, column=0, columnspan=3, sticky='nswe')
        label_exp8.grid(row=15, column=0, columnspan=1, sticky='nswe')
        texp_entry8.grid(row=15, column=1, columnspan=1, sticky='nswe')
        label_units8.grid(row=15, column=2, columnspan=1, sticky='nswe')
        spacer8.grid(row=16, column=0, columnspan=3, sticky='nswe')

        ''' f2 - Gratings '''
        sp0 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check1 = tk.Label(f2, text='Grating 1:')
        self.isChecked1 = tk.IntVar()
        self.check1 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked1,
                                 onvalue=1, offvalue=0)
        sp1 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check2 = tk.Label(f2, text='Polystyrene 1:')
        self.isChecked2 = tk.IntVar()
        self.check2 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked2,
                                 onvalue=1, offvalue=0)
        sp2 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check3 = tk.Label(f2, text='Blank 1:')
        self.isChecked3 = tk.IntVar()
        self.check3 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked3,
                                 onvalue=1, offvalue=0)
        sp3 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check4 = tk.Label(f2, text='Grating 2:')
        self.isChecked4 = tk.IntVar()
        self.check4 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked4,
                                 onvalue=1, offvalue=0)
        sp4 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check5 = tk.Label(f2, text='Polystyrene 2:')
        self.isChecked5 = tk.IntVar()
        self.check5 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked5,
                                 onvalue=1, offvalue=0)
        sp5 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check6 = tk.Label(f2, text='Blank 2:')
        self.isChecked6 = tk.IntVar()
        self.check6 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked6,
                                 onvalue=1, offvalue=0)
        sp6 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check7 = tk.Label(f2, text='Grating 3:')
        self.isChecked7 = tk.IntVar()
        self.check7 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked7,
                                 onvalue=1, offvalue=0)
        sp7 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check8 = tk.Label(f2, text='Polystyrene 3:')
        self.isChecked8 = tk.IntVar()
        self.check8 = ttk.Checkbutton(f2, text='Use this grating?', variable=self.isChecked8,
                                 onvalue=1, offvalue=0)
        sp8 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        sp9 = tk.Label(f2, height=1, text='') # VERTICAL SPACER
        label_check_all = tk.Label(f2, text='Select all:')
        self.isChecked9 = tk.IntVar()
        self.check_all = ttk.Checkbutton(f2, text='Select all gratings?',
                                         variable=self.isChecked9,
                                         onvalue=1, offvalue=0,
                                         command=self.select_all_gratings)
        sp10 = tk.Label(f2, height=1, text='') # VERTICAL SPACER

        # set the default selected gratings as indicated in "selected.csv" file
        if sel:
            self.isChecked1.set(sel[0])
            self.isChecked2.set(sel[1])
            self.isChecked3.set(sel[2])
            self.isChecked4.set(sel[3])
            self.isChecked5.set(sel[4])
            self.isChecked6.set(sel[5])
            self.isChecked7.set(sel[6])
            self.isChecked8.set(sel[7])
        
        sp0.grid(row=0, column=0, columnspan=2, sticky='nswe')
        label_check1.grid(row=1, column=0, columnspan=1, sticky='nswe')
        self.check1.grid(row=1, column=1, columnspan=1, sticky='nswe')
        sp1.grid(row=2, column=0, columnspan=2, sticky='nswe')
        label_check2.grid(row=3, column=0, columnspan=1, sticky='nswe')
        self.check2.grid(row=3, column=1, columnspan=1, sticky='nswe')
        sp2.grid(row=4, column=0, columnspan=2, sticky='nswe')
        label_check3.grid(row=5, column=0, columnspan=1, sticky='nswe')
        self.check3.grid(row=5, column=1, columnspan=1, sticky='nswe')
        sp3.grid(row=6, column=0, columnspan=2, sticky='nswe')
        label_check4.grid(row=7, column=0, columnspan=1, sticky='nswe')
        self.check4.grid(row=7, column=1, columnspan=1, sticky='nswe')
        sp4.grid(row=8, column=0, columnspan=2, sticky='nswe')
        label_check5.grid(row=9, column=0, columnspan=1, sticky='nswe')
        self.check5.grid(row=9, column=1, columnspan=1, sticky='nswe')
        sp5.grid(row=10, column=0, columnspan=2, sticky='nswe')
        label_check6.grid(row=11, column=0, columnspan=1, sticky='nswe')
        self.check6.grid(row=11, column=1, columnspan=1, sticky='nswe')
        sp6.grid(row=12, column=0, columnspan=2, sticky='nswe')
        label_check7.grid(row=13, column=0, columnspan=1, sticky='nswe')
        self.check7.grid(row=13, column=1, columnspan=1, sticky='nswe')
        sp7.grid(row=14, column=0, columnspan=2, sticky='nswe')
        label_check8.grid(row=15, column=0, columnspan=1, sticky='nswe')
        self.check8.grid(row=15, column=1, columnspan=1, sticky='nswe')
        sp8.grid(row=16, column=0, columnspan=2, sticky='nswe')
        sp9.grid(row=17, column=0, columnspan=2, sticky='nswe')
        label_check_all.grid(row=18, column=0, columnspan=1, sticky='nswe')
        self.check_all.grid(row=18, column=1, columnspan=1, sticky='nswe')
        sp10.grid(row=19, column=0, columnspan=2, sticky='nswe')

        ''' f3 - Focus '''
        self.incAbg = '#0099cc'
        self.incBbg = '#ff6600'
        self.incCbg = '#66ff1a'
        self.incDbg = '#e6e600'
        stg1_spc_1 = tk.Label(f3, height=1, text=' ')
        s1_plusA_button = tk.Button(f3, text="+ tiny",\
                                    font=("Calibri", 10),\
                                    fg='black', bg=self.incAbg,\
                                    height=1, width=5, command=self._stg1_plusA)
        s1_plusB_button = tk.Button(f3, text="+ nom",\
                                    font=("Calibri", 10),\
                                    fg='black', bg=self.incBbg,\
                                    height=2, width=5, command=self._stg1_plusB)
        s1_plusC_button = tk.Button(f3, text="+ big",\
                                    font=("Calibri", 10),\
                                    fg='black', bg=self.incCbg,\
                                    height=3, width=5, command=self._stg1_plusC)
        stg1_spc_2 = tk.Label(f3, height=1, text=' ')
        s1_minusC_button = tk.Button(f3, text="- big",\
                                    font=("Calibri", 10),\
                                    fg='black', bg=self.incCbg,\
                                    height=3, width=5, command=self._stg1_minusC)
        s1_minusB_button = tk.Button(f3, text="- nom",\
                                    font=("Calibri", 10),\
                                    fg='black', bg=self.incBbg,\
                                    height=2, width=5, command=self._stg1_minusB)
        s1_minusA_button = tk.Button(f3, text="- tiny",\
                                    font=("Calibri", 10),\
                                    fg='black', bg=self.incAbg,\
                                    height=1, width=5, command=self._stg1_minusA)
        stg1_spc_3 = tk.Label(f3, height=1, text=' ')

        '''
        Layout the widgets within their frames
        '''
        # stage_1_frame
        stg1_spc_1.grid(row=0, column=0, sticky='nswe')
        s1_plusA_button.grid(row=1, column=0, sticky='nswe')
        s1_plusB_button.grid(row=2, column=0, sticky='nswe')
        s1_plusC_button.grid(row=3, column=0, sticky='nswe')
        stg1_spc_2.grid(row=4, column=0, sticky='nswe')
        s1_minusC_button.grid(row=5, column=0, sticky='nswe')
        s1_minusB_button.grid(row=6, column=0, sticky='nswe')
        s1_minusA_button.grid(row=7, column=0, sticky='nswe')
        stg1_spc_3.grid(row=8, column=0, sticky='nswe')

        ''' f4 - Frames '''
        frames_spc_1 = tk.Label(f4, height=1, text=' ')
        label_nb_frames = tk.Label(f4, text='Number of frames to grab')
        self.nb_frames = tk.IntVar()
        self.nb_frames.set(nframes)
        self.nb_frames_entry = ttk.Entry(f4, width=8, textvariable=self.nb_frames)
        frames_spc_2 = tk.Label(f4, height=1, text=' ')

        '''
        Layout the widgets within their frames
        '''
        frames_spc_1.grid(row=0, column=0, sticky='nswe')
        label_nb_frames.grid(row=1, column=0, sticky='nswe')
        self.nb_frames_entry.grid(row=2, column=0, sticky='nswe')
        frames_spc_2.grid(row=3, column=0, sticky='nswe')

        # close notebook button
        button_apply_close = tk.Button(self.window, text="Apply & Close", command=self.apply_close) 
        n.grid(row=1, column=0)
        button_apply_close.grid(row=2, column=0)

    def _stg1_plusA(self):
        command = "STAGE1_RELATIVE_MOVE_TINY_POSITIVE"
        self.to_micronix_queue.put(command)

    def _stg1_plusB(self):
        command = "STAGE1_RELATIVE_MOVE_NOMINAL_POSITIVE"
        self.to_micronix_queue.put(command)

    def _stg1_plusC(self):
        command = "STAGE1_RELATIVE_MOVE_BIG_POSITIVE"
        self.to_micronix_queue.put(command)

    def _stg1_minusC(self):
        command = "STAGE1_RELATIVE_MOVE_BIG_NEGATIVE"
        self.to_micronix_queue.put(command)

    def _stg1_minusB(self):
        command = "STAGE1_RELATIVE_MOVE_NOMINAL_NEGATIVE"
        self.to_micronix_queue.put(command)

    def _stg1_minusA(self):
        command = "STAGE1_RELATIVE_MOVE_TINY_NEGATIVE"
        self.to_micronix_queue.put(command)

    def select_all_gratings(self):
        if self.isChecked9.get():
            self.isChecked1.set(True)
            self.isChecked2.set(True)
            self.isChecked3.set(True)
            self.isChecked4.set(True)
            self.isChecked5.set(True)
            self.isChecked6.set(True)
            self.isChecked7.set(True)
            self.isChecked8.set(True)
        else:
            self.isChecked1.set(False)
            self.isChecked2.set(False)
            self.isChecked3.set(False)
            self.isChecked4.set(False)
            self.isChecked5.set(False)
            self.isChecked6.set(False)
            self.isChecked7.set(False)
            self.isChecked8.set(False)

    def apply_close(self):
        #save the settings values to csv file
        write_exposures([self.texp1.get(), self.texp2.get(), self.texp3.get(), self.texp4.get(),
                         self.texp5.get(), self.texp6.get(), self.texp7.get(), self.texp8.get()])
        write_gratings_selected([self.isChecked1.get(), self.isChecked2.get(),
                                 self.isChecked3.get(), self.isChecked4.get(),     
                                 self.isChecked5.get(), self.isChecked6.get(),
                                 self.isChecked7.get(), self.isChecked8.get()])
        write_frames([self.nb_frames.get()])
        # finally, close this notebook window
        self.window.destroy()


class Tk_Handler():

    def __init__(self, root, canvas, canvas2, line):
        self.root = root
        self.line = line
        self.cryo_queue = queue.Queue()
        self.stage_ready = False # if stage is moving, then don't collect data
        self.root_dir = Path(__file__).parent.absolute()
        self.config_dir = os.path.join(self.root_dir, 'Config')
        self.root_data_dir = os.path.join(self.root_dir, 'Data')
        self.subdirpath = ''
        self.data_prefix = None # user-defined run description
        self.dset_name = None # user-defined run subset identifier
        self.measurement_type = None # user must select either "sample" or "calibration"
        self.logfile = None
        self.templogfile = None
        self.stage_position = "" # user selected stage position
        self.plottitle = "" # the plot title is different for live and saved data
        self.nb_lines = 1
        self.data_dir = "" # data dir name based on time+date and run description
        self.has_colorbar = False
        self.showing_full_image = True
        self.root.wm_title("Spectrometer Control & Data Collect Interface")
        #self.stage_controller = stage_controller.StageController()

        '''
        Camera
        '''
        self.nb_frames = 128
        self.single_image = np.zeros((self.nb_frames,128,128), dtype=np.uint16)
        self.single_image_path = self.root_dir
        self.camera_command = "SINGLE_FRAME"
        self.single_file_name = "SINGLE_FRAME.RAW"
        self.n_frames_file_name = None
        self.cam_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.to_camera_queue = queue.Queue()
        self.from_camera_queue = queue.Queue()
        if VIRTUAL_CAMERA:
            self.camera = camera.CameraVirtual(self.to_camera_queue, self.from_camera_queue,
                                               self.single_image_path)
        else:
            self._start_camera_communication()
            self.camera = camera.Camera(self.to_camera_queue, self.from_camera_queue,
                                        self.single_image_path)
        self.camera.start()

        '''
        Micronix Stage-motors controller
        '''
        self.micronix_command = ""
        self.to_micronix_queue = queue.Queue()
        self.from_micronix_queue = queue.Queue()
        if VIRTUAL_STAGE_CONTROLLER:
            self.micronix = stage_controller.VirtualStageController(self.to_micronix_queue,
                                                                    self.from_micronix_queue)
        else:
            self.micronix = stage_controller.StageController(self.to_micronix_queue,
                                                             self.from_micronix_queue,
                                                             find_center=FIND_CENTER)
        self.micronix.start()

        # begin showing the live single-grab images
        self.SImg = ShowImage(self.root, self.has_colorbar, self.single_image)

        '''
        Create the main containers
        '''
        controls_frame = tk.Frame(self.root)    # upper, leftmost column
        config_frame = tk.Frame(self.root)      # middle, leftmost column; for config button
        quit_frame = tk.Frame(self.root)        # lower, leftmost column
        camera_frame = tk.Frame(self.root)      # shows live camera images
        plot_frame = tk.Frame(self.root)        # shows spectral plots
        cam_buttons_frame = tk.Frame(self.root) # below camera_frame
        plt_buttons_frame = tk.Frame(self.root) # below plot_frame
        text_frame = tk.Frame(self.root)        # text display area at bottom

        '''
        Layout the main containers
        '''
        tk.Grid.rowconfigure(self.root, 0, weight=1)
        tk.Grid.columnconfigure(self.root, 0, weight=1)
        controls_frame.grid(row=0, column=0, pady=0, sticky='n')
        config_frame.grid(row=1, column=0, pady=0)
        quit_frame.grid(row=2, column=0, pady=0)
        camera_frame.grid(row=0, column=1, pady=0)
        plot_frame.grid(row=0, column=2, pady=0)
        cam_buttons_frame.grid(row=1, column=1, pady=0)
        plt_buttons_frame.grid(row=1, column=2, pady=0)
        text_frame.grid(row=2, column=1, columnspan=2, pady=0, sticky='we')

        '''
        Create the Widgets for the Frames
        '''
        # controls_frame - Create the widgets
        spacer1 = tk.Label(controls_frame, height=1, text=' ') # VERTICAL SPACER
        label_desc = tk.Label(controls_frame, text='Enter Data Description')
        dataDesc = tk.StringVar()
        self.description_entry = ttk.Entry(controls_frame, width=15, textvariable=dataDesc)
        spacer2 = tk.Label(controls_frame, height=1, text=' ') # VERTICAL SPACER

        label_dset = tk.Label(controls_frame, text='Set Name')
        self.dsetName = tk.StringVar()
        self.dset_entry = ttk.Entry(controls_frame, width=8, textvariable=self.dsetName)
        spacer3 = tk.Label(controls_frame, height=1, text=' ') # VERTICAL SPACER
        
        label_col = tk.Label(controls_frame, text='Select Measurement Type')  
        measType = tk.StringVar()
        self.col_cbox = ttk.Combobox(controls_frame, width=13, textvariable=measType)
        self.col_cbox['values'] = ('sample', 'calibration')
        self.col_cbox.bind("<<ComboboxSelected>>", self._set_measurement_type)
        spacer4 = tk.Label(controls_frame, height=2, text='\n') # VERTICAL SPACER

        panel_width = 100
        radius = 30
        self.panelbg = '#663300'
        self.ledbg = '#669999'
        self.red = '#ff1a1a'
        self.yellow = '#ffff33'
        self.green = 'limegreen'
        stg_1_label = tk.Label(controls_frame, text='Camera focus stage', font=("Calibri", 13))
        self.stg_1_canvas = tk.Canvas(controls_frame, width=panel_width, height=40, bg=self.panelbg)
        self.led_red_1 = self.stg_1_canvas.create_oval(10, 10, radius, radius, fill=self.ledbg) 
        self.stg_1_canvas.itemconfig(self.led_red_1, fill=self.ledbg)
        self.led_yellow_1 = self.stg_1_canvas.create_oval(10+radius, 10, radius*2, radius, fill=self.ledbg) 
        self.stg_1_canvas.itemconfig(self.led_yellow_1, fill=self.ledbg)
        self.led_green_1 = self.stg_1_canvas.create_oval(10+radius*2, 10, radius*3, radius, fill=self.ledbg) 
        self.stg_1_canvas.itemconfig(self.led_green_1, fill=self.ledbg)         
        spacer5 = tk.Label(controls_frame, height=2, text='\n')

        stg_2_label = tk.Label(controls_frame, text='Aperture Stage', font=("Calibri", 13))
        self.stg_2_canvas = tk.Canvas(controls_frame, width=panel_width, height=40, bg=self.panelbg)
        self.led_red_2 = self.stg_2_canvas.create_oval(10, 10, radius, radius, fill=self.ledbg) 
        self.stg_2_canvas.itemconfig(self.led_red_2, fill=self.ledbg)
        self.led_yellow_2 = self.stg_2_canvas.create_oval(10+radius, 10, radius*2, radius, fill=self.ledbg) 
        self.stg_2_canvas.itemconfig(self.led_yellow_2, fill=self.ledbg)
        self.led_green_2 = self.stg_2_canvas.create_oval(10+radius*2, 10, radius*3, radius, fill=self.ledbg) 
        self.stg_2_canvas.itemconfig(self.led_green_2, fill=self.ledbg)
        spacer6 = tk.Label(controls_frame, height=2, text='\n')

        self.acq_button = tk.Button(controls_frame, width=15, height=3,
                                    command=self._collect_spectral_data) # _move_stage, then _get_spec_data
        self.acq_button['text'] = 'Acquire Spectral Data\n at\n Selected Stage Positions'
        if MANUAL_GRAB_OPTION:
            spacer7 = tk.Label(controls_frame, height=5, text='\n') # VERTICAL SPACER
            self.grab_button = tk.Button(controls_frame, width=12, height=2,
                                         bg='lightgray', fg='blue',
                                         command=self._grab_n_frames)
            self.grab_button['text'] = 'TEST\nGrab N Frames'
            spacer8 = tk.Label(controls_frame, height=2, text='\n') # VERTICAL SPACER
        

        # config_frame - Create the widgets
        button_popup = tk.Button(config_frame, text='Configure',\
                                 bg='white', fg='black',\
                                 font=("Calibri", 13),\
                                 command=self._popup_nb)

        # quit_frame - Create the widgets
        quit_button = tk.Button(quit_frame, text="Quit",\
                                font=("Calibri", 15),\
                                fg='white', bg='darkred',\
                                height=2, width=5, command=self._quit)

        # camera_frame - Create the widgets
        # None, using a canvas object

        # plot_frame - Create the widgets
        # None, using a canvas object

        # cam_buttons_frame - Create the widgets
        full_image_button = tk.Button(cam_buttons_frame, text="Show full image",\
                                      height=2, command = self._show_full_image)
        cropped_image_button = tk.Button(cam_buttons_frame, text="Show cropped image",\
                                         height=2, command = self._show_cropped_image)

        # plt_buttons_frame - Create the widgets
        saved_single_button = tk.Button(plt_buttons_frame, text="Plot saved single curve",\
                                        height=2, command=self._view_saved_single)
        saved_combined_button = tk.Button(plt_buttons_frame, text="Plot saved combined curve",\
                                        height=2, command=self._view_saved_combined)


        # text_frame - Create the widgets
        self.text_area = scrolledtext.ScrolledText(text_frame, wrap=tk.WORD,
                                              height=7, width=160, font=("Calibri", 11),
                                              bg='black', fg=LOGGING_COLOR)
        self.text_area.insert(tk.INSERT, "log messages ...\n")
        self.text_area.tag_config('warning', background="yellow", foreground="red")
        #self.text_area.configure(state ='disabled') # make the text read-only


        '''
        Layout the widgets within their frames
        '''
        # controls_frame
        spacer1.grid(row=0, column=0, columnspan=1, sticky='nswe')
        label_desc.grid(row=1, column=0, columnspan=1, sticky='nswe')
        self.description_entry.grid(row=2, column=0, columnspan=1, sticky='nswe')
        spacer2.grid(row=3, column=0, columnspan=1, sticky='nswe')

        label_dset.grid(row=4, column=0, columnspan=1, sticky='nswe')
        self.dset_entry.grid(row=5, column=0, columnspan=1)
        spacer3.grid(row=6, column=0, columnspan=1, sticky='nswe')

        label_col.grid(row=7, column=0, columnspan=1, sticky='nswe')
        self.col_cbox.grid(row=8, column=0, columnspan=1, sticky='nswe')
        spacer4.grid(row=9, column=0, columnspan=1, sticky='nswe')

        stg_1_label.grid(row=10, column=0, columnspan=1)
        self.stg_1_canvas.grid(row=11, column=0, columnspan=1)
        spacer5.grid(row=12, column=0, columnspan=1, sticky='nswe')
       
        stg_2_label.grid(row=13, column=0, columnspan=1)
        self.stg_2_canvas.grid(row=14, column=0, columnspan=1)
        spacer6.grid(row=15, column=0, columnspan=1, sticky='nswe')
       
        self.acq_button.grid(row=16, column=0, columnspan=1, sticky='nswe')
        if MANUAL_GRAB_OPTION: 
            spacer7.grid(row=17, column=0, columnspan=1, sticky='nswe')
            self.grab_button.grid(row=18, column=0, columnspan=1, sticky='nswe')
            spacer8.grid(row=19, column=0, columnspan=1, sticky='nswe')        

        # config_frame
        button_popup.grid(row=0, column=0, columnspan=1)

        # quit_frame
        quit_button.grid(row=0, column=0, columnspan=1)

        # camera_frame
        canvas2.get_tk_widget().grid(row=0, column=1, rowspan=1, columnspan=1,
                                     padx=6, pady=0, sticky='nswe')

        # plot_frame
        canvas.get_tk_widget().grid(row=0, column=2, rowspan=1, columnspan=1,
                                    padx=0, pady=0, sticky='nsew')

        # cam_buttons_frame
        full_image_button.grid(row=0, column=0, padx=2)
        cropped_image_button.grid(row=0, column=1, padx=0)

        # plt_buttons_frame
        saved_single_button.grid(row=0, column=0, padx=2)
        saved_combined_button.grid(row=0, column=1, padx=0)

        # text_frame
        self.text_area.grid(row=1, column=1, columnspan=2, pady=0, sticky='nswe')

        # UDP Listener
        UDP_IP = "127.0.0.1"
        UDP_PORT = 20001
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind((UDP_IP, UDP_PORT))
        # Create and start receiver thread
        self.receiver = Receiver(self.sock, self.cryo_queue)
        self.receiver.start()
        
        # Check queues periodically for a new message
        self.periodicCall()
        
        tk.mainloop()

    def _quit(self):
        self.camera.stop()   # exit camera communication software and terminate the camera thread
        self.camera.join()   # "
        self.camera = None   # "
        self.micronix.stop()   # terminate the micronix thread
        self.micronix.join()   # "
        self.micronix = None   # "
        self.receiver.stop() # terminate the cryo-controller UDP listener thread
        self.receiver.join() # "
        self.receiver = None # "
        self.root.quit()     # stops mainloop
        self.root.destroy()  # "

    def _start_camera_communication(self):
        pass
        #import subprocess
        #s1 = R'''c:\Documents\Teledyne Scientific & Imaging\IPS '''
        #s2 = R'''SDK\Camera\Debug\Camera.exe'''
        #cmdstr = s1 + s2
        #subprocess.Popen([cmdstr])
        #time.sleep(3)

    def _popup_nb(self):
        self.configurator = ConfigurationNotebook(self.to_micronix_queue)
        #self.camera.nb_frames_to_grab = self.configurator.nb_frames

    def _collect_spectral_data(self):
        self.nb_lines = 1
        self.data_prefix = self.description_entry.get()
        self.measurement_type = self.col_cbox.get()
        self.dset_name = self.dsetName.get()

        if self.logfile != None:
            if self.dset_name != None:
                cancel_collect = self._confirmSet(self.dset_name) # was this set name already used?
                if cancel_collect:
                    messagebox.showinfo( "Unallowed Set Name",
                                        "Set name already used; please either enter a new one" +
                                        " or delete the CSV files with this set name." )
                    return 0
                self.logfile.append( get_HMS() + " - Collect Spectral Data" )
                self.logfile.append(11*" " + "prefix: " +  self.data_prefix +
                                    ", set name: " + self.dset_name +
                                    ", measurement type: " + self.measurement_type) 

            else:
                messagebox.showinfo( "Required Information Missing", "Please enter a Set name")
                return 0
        
            self.acq_button['state'] = 'disabled'
            # read the config files
            exps, exps_prev, sel, sel_prev, n_frames = read_config_files_csv("Config")
            self.nb_frames = int(n_frames)
            for i,x in enumerate(sel):
                stage_pos = i+1
                if x != 0:
                    self.stage_position = stage_pos

                    self.logfile.append( get_HMS() + " - move to stage position: " +
                                         str(self.stage_position) )
                    self.text_area.insert(tk.INSERT, "move to stage position: "+
                                                      str(self.stage_position) + "\n" )
                    self.text_area.see("end") # scroll down
                    self._moveStage() # send command to move the stage
                    self._get_spec_data() # grab N frames, then read and display
            self.acq_button['state'] = 'active'
        else:
            # promt user to define Data Description and Measurement Type
            messagebox.showinfo( "Required Information Missing",
                    "Please enter a Data Description and select a Measurement Type." )

    def _confirmSet(self, thisSet):
        # get a unique list of the set names found in this data_prefix directory
        prefix = self.data_prefix
        dset = self.dset_name
        meas_type = self.measurement_type
        stage_pos = self.stage_position
        subdirpath = Path(os.path.join(self.root_data_dir, prefix))
        subdirpath = Path(os.path.join(subdirpath, 'Spectral'))
        subdirpath = Path(os.path.join(subdirpath, meas_type)) 
        onlyfiles = [f for f in os.listdir(subdirpath)
                    if os.path.isfile(os.path.join(subdirpath, f))]
        csv_files = [x for x in onlyfiles if x.endswith(".csv")]
        set_names = list(set([x.split("_")[0] for x in csv_files]))
        if thisSet in set_names:
            return True # this set name already used
        else:
            return False # this set name not already used

    def _get_spec_data(self):
        timestr = time.strftime("%Y%m%d-%H%M%S")
        thisDset = self.dset_name + '_'+timestr+'_'+str(self.stage_position).zfill(2)
        subdirpath = Path(os.path.join(self.root_data_dir, self.data_prefix))
        subdirpath.mkdir(parents=True, exist_ok=True)
        subdirpath = Path(os.path.join(subdirpath, 'Spectral'))
        subdirpath.mkdir(parents=True, exist_ok=True)
        subdirpath = Path(os.path.join(subdirpath, self.measurement_type))
        subdirpath.mkdir(parents=True, exist_ok=True)
    
        # grab N frames & write to disc
        #dset = self.dset_name+'_'+timestr+'_'+str(stage_pos).zfill(2)
        #filepath = os.path.join(subdirpath, dset+'_'+timestr+'_'+str(stage_pos).zfill(2))
        self.camera.set_data_dir(subdirpath)
        self.camera.set_dset(thisDset)
        self.camera.nb_frames_to_grab = self.nb_frames
        self.to_camera_queue.put("N_FRAMES_MEASUREMENT")

        # before attempting to read the image, make sure it has been writen
        found = False
        start_time = time.time()
        thresh = 5 # allow up to thresh seconds to receive OK message from camera
        elapsed_time = 0
        while (elapsed_time < thresh) and (found == False):
            while self.from_camera_queue.qsize():
                try:
                    msg = self.from_camera_queue.get()
                    if (msg == "OK NFRAMES MEASUREMENT"):
                        found = True
                    else:
                        time.sleep(0.05)
                except queue.Empty:
                    pass
            elapsed_time = time.time() - start_time                

        #if count == Ntrys:
        #    messagebox.showinfo( "File Missing", "Did not find NFRAMES.RAW") 
        #    return 0

        #img = np.zeros((128,128,128), np.uint16)
        filepath = os.path.join(subdirpath, thisDset+"_NFRAMES.RAW")
        img = np.fromfile(filepath, np.uint16).reshape((self.nb_frames,128,128))

        # calulate temporal mean
        Img = np.mean(img, axis=0).astype(np.uint16)
        Img = Img.astype(np.uint16)
        print( "shape: ", Img.shape, "  mean: ", "%0.2f" % np.mean(Img),
               "  min: ", np.min(Img), "  max: ", np.max(Img) )

        # apply Gains & Offsets
        print("applying NUC ...")
        nucfile = os.path.join(self.config_dir, "NUC.raw")
        if os.path.exists(nucfile):
            nuc = np.fromfile(nucfile, np.float64).reshape((4,128,128))
            Img = nuc[0]*(Img**3) + nuc[1]*(Img**2) + nuc[2]*(Img) + nuc[3]
        else:
            messagebox.showinfo( "File Missing", "Did not find the file: NUC.raw")
            return 0

        # apply BPR
        print("applying BPR ...")
        bprfile = os.path.join(self.config_dir, "BPR.csv")
        if os.path.exists(bprfile):
            idx = []
            with open(bprfile, "r") as f:
                reader = csv.reader(f, delimiter=",")
                for i, line in enumerate(reader):
                    if i>0: # skipping header
                        idx.append([int(x) for x in line])
            Img = Img.flatten()
            for x in idx: # x[0] = bad idx, x[1] = replacement value
                Img[x[0]] = x[1]
            Img = Img.reshape((128,128))
        else:
            messagebox.showinfo( "File Missing", "Did not find the file: BPR.csv")
            return 0

        # apply Radiance Correction
        print("applying RAD ...")
        radfile = os.path.join(self.config_dir, "RAD.raw")
        if os.path.exists(radfile):
            rad = np.fromfile(radfile, np.float64).reshape((4,128,128))
            Img = (rad[2] * (rad[0]*Img + rad[1])) + rad[3]
        else:
            messagebox.showinfo("File Missing", "Did not find the file: RAD.raw")
            return 0
             
        # make line data for plotting and for writing to csv
        ###############################################################
        # some fake processing of the image data for now
        valarr = Img[63,13:113]
        if self.stage_position == 1:
            ys = valarr * 1.0
        elif self.stage_position == 2:
            ys = valarr * 1.1
        elif self.stage_position == 3:
            ys = valarr * 1.2
        elif self.stage_position == 4:
            ys = valarr * 1.3
        elif self.stage_position == 5:
            ys = valarr * 1.4
        elif self.stage_position == 6:
            ys = valarr * 1.5
        elif self.stage_position == 7:
            ys = valarr * 1.6
        elif self.stage_position == 8:
            ys = valarr * 1.7
        xs = [x for x in range(0,100,1)]
        ###############################################################
        # plot the data
        self.line.y = ys
        self.line.x = xs
        self.plottitle = 'Spectral Curve'
        self.line.draw_plot(self.plottitle, self.nb_lines)
        # write data to csv file
        write_spectral_data(xs, ys, self.data_prefix, self.dset_name,
                            self.stage_position, self.measurement_type)

    def _moveStage(self):
        self._set_stg2_yellow() # change LED color to yellow: busy
        pos = self.stage_position
        self.micronix_command = "STAGE2_MOVE_TO_APERTURE_" + str(pos)
        self.to_micronix_queue.put(self.micronix_command)
        # wait for received confirmation from stage_controller, or until timeout
        WAITING = True
        timeout_secs = 5 # maximum time to wait for confirmation from stage_controller
        start_time = time.time()
        while (WAITING):
            while self.from_micronix_queue.qsize():
                try:
                    msg = self.from_micronix_queue.get()
                    print(msg)
                    WAITING = False 
                except queue.Empty:
                    pass
            elapsed_time = time.time() - start_time
            if elapsed_time > timeout_secs:
                print("TIMED OUT after 5 seconds.")
                WAITING = False
        
    def _grab_n_frames(self): # method for the front panel "manual grab" button
        self.dset_name = self.dsetName.get()
        self.camera.set_dset(self.dset_name)
        self.camera.set_data_dir(self.root_dir)
        self.n_frames_file_name = "NFRAMES.RAW"
        self.camera_command = "N_FRAMES"

    def _set_measurement_type(self, event):
        # only execute if measurement_type AND data_prefix have changed
        if not ( (self.col_cbox.get() == self.measurement_type) and
                 (self.description_entry.get() == self.data_prefix) ):

            self.measurement_type = self.col_cbox.get()
            self.text_area.insert( tk.INSERT, get_current_time() + " - " +
                                              "setting measurement type: "+
                                              self.measurement_type +'\n' )
            self.text_area.see("end")
            # open the logfile for this measurement_type
            self.data_prefix = self.description_entry.get()
            self.logfile = Logfile(self.data_prefix, self.measurement_type)
            # open the temperature logfile for this measurement_type
            self.templogfile = Templogfile(self.data_prefix, self.measurement_type)

    def _view_saved_single(self):
        # select the file
        filename = filedialog.askopenfilename(initialdir='Data',
                                              filetypes=[("csv files", "*.csv")])
        # split path into a head and tail, keep the tail
        tail = os.path.split(filename)[-1]
        pos = tail.split('_')[-1].split('.')[0]
        dset = tail.split('_')[0]
        # open and display plot
        I1 = []
        W1 = []
        rcounter = 0
        with open(filename) as csvfile:
            savedreader = csv.reader(csvfile, delimiter=',')
            for row in savedreader:
                if rcounter > 1:
                    (i1,w1) = row
                    I1.append(int(i1))
                    W1.append(float(w1))
                rcounter += 1
        
        xs = np.array(I1, dtype=np.uint64)
        ys = np.array(W1, dtype=np.uint64)
        self.line.x = xs
        self.line.y = ys
        self.plottitle = dset + ' - position: ' + pos
        self.nb_lines = 1
        self.line.draw_plot(self.plottitle, self.nb_lines)

    def _view_saved_combined(self):
        filename = filedialog.askopenfilename(initialdir='Data',
                                              filetypes=[("csv files", "*.csv")],
                                title='Select one of the files from the data-set of interest.')
        # split path into a head and tail, keep the tail
        head, tail = os.path.split(filename)
        meas = os.path.split(head)[-1]
        dset = tail.split('_')[0]
        onlyfiles = [f for f in os.listdir(head)
                     if os.path.isfile(os.path.join(head, f))] 
        csv_files = sorted([ x for x in onlyfiles
                             if x.endswith('.csv') and
                             x.startswith(dset) ])
        #print(csv_files)
        if csv_files:
            self.subdirpath = head
            for _ in csv_files:
                ys, xs, nb_lines = get_selected_csv(self.subdirpath, dset)

            self.nb_lines = nb_lines

            # plot the data
            self.line.y = ys
            self.line.x = xs
            self.plottitle = dset + ' - ' + meas + ' - combined spectral curve'
            self.line.draw_plot(self.plottitle, self.nb_lines)
                 
    def _show_full_image(self):
        self.SImg.cancel_self()
        self.SImg = ShowImage(self.root, self.has_colorbar, self.single_image)
        self.showing_full_image = True

    def _show_cropped_image(self):
        self.SImg.cancel_self()
        self.SImg = ShowCroppedImage(self.root, self.single_image)
        self.showing_full_image = False

    def processCryoIncoming(self):
        """Handle all messages currently in the cryo_queue, if any."""
        while self.cryo_queue.qsize():
            try:
                msg = self.cryo_queue.get().decode("utf-8")
                if (msg.startswith("WARNING")) or (msg.startswith("ERROR")):
                    self.text_area.insert( tk.INSERT, get_current_time() + " - " +
                                           msg +'\n', 'warning' )
                    if self.logfile != None:
                        self.logfile.append( get_HMS() + " - UDP message: " + msg )
                else:
                    self.text_area.insert( tk.INSERT, get_current_time() + " - " + msg +'\n' )
                    if self.templogfile != None:
                        self.templogfile.append(msg)

                self.text_area.see("end")
            except queue.Empty:
                pass

    def processCameraIncoming(self):
        #print("sending: ", self.camera_command)
        self.to_camera_queue.put(self.camera_command)
        while self.from_camera_queue.qsize():
            try:
                msg = self.from_camera_queue.get()
                if (msg == "OK SINGLE"):
                    #print(" ---> SINGLE FRAME")
                    # read the current image from file
                    filepath = os.path.join(self.single_image_path, self.single_file_name)
                    self.single_image = np.fromfile(filepath, dtype=np.uint16)
 
                if (msg == "OK NFRAMES"):
                    #print(" ---> NFRAMES")
                    # prompt user for a path for saving the nframes
                    filepath = filedialog.asksaveasfilename(title="save as")
                    if len(filepath) == 0:
                        messagebox.showinfo("Invalid Path", "An invalid path was given; not saving.")
                        break
                        
                    # read the current image from file
                    filepath = filepath + ".RAW"
                    temppath = os.path.join(self.root_dir, "MANUAL_NFRAMES.RAW")
                    seq = np.fromfile(temppath, dtype=np.uint16)
                    seq.astype('int16').tofile(filepath)

                    # after the nframes grab, switch back to single-grab mode
                    self.camera_command = "SINGLE_FRAME"
    
            except queue.Empty:
                pass

    def processMicronixIncoming(self):
        self.to_micronix_queue.put(self.micronix_command)
        self.micronix_command = ""
        while self.from_micronix_queue.qsize():
            try:
                msg = self.from_micronix_queue.get()
                if (msg == "STAGE 1 OK"): self._set_stg1_green() 
                if (msg == "STAGE 2 OK"): self._set_stg2_green()
                if (msg == "STAGE 1 ERROR"): self._set_stg1_red() 
                if (msg == "STAGE 2 ERROR"): self._set_stg2_red()  
            except queue.Empty:
                pass
        
    def periodicCall(self):
        """
        Check cryo-controller message queue every 100 ms.
        """
        self.processCryoIncoming()
        self.processCameraIncoming()
        self.processMicronixIncoming()
        self.root.after(100, self.periodicCall)

    def _set_stg1_red(self):
        self.stg_1_canvas.itemconfig(self.led_red_1, fill=self.red)
        self.stg_1_canvas.itemconfig(self.led_yellow_1, fill=self.ledbg)
        self.stg_1_canvas.itemconfig(self.led_green_1, fill=self.ledbg)

    def _set_stg1_yellow(self):
        self.stg_1_canvas.itemconfig(self.led_red_1, fill=self.ledbg)
        self.stg_1_canvas.itemconfig(self.led_yellow_1, fill=self.yellow)
        self.stg_1_canvas.itemconfig(self.led_green_1, fill=self.ledbg)

    def _set_stg1_green(self):
        self.stg_1_canvas.itemconfig(self.led_red_1, fill=self.ledbg)
        self.stg_1_canvas.itemconfig(self.led_yellow_1, fill=self.ledbg)
        self.stg_1_canvas.itemconfig(self.led_green_1, fill=self.green)

    def _set_stg2_red(self):
        self.stg_2_canvas.itemconfig(self.led_red_2, fill=self.red)
        self.stg_2_canvas.itemconfig(self.led_yellow_2, fill=self.ledbg)
        self.stg_2_canvas.itemconfig(self.led_green_2, fill=self.ledbg)

    def _set_stg2_yellow(self):
        self.stg_2_canvas.itemconfig(self.led_red_2, fill=self.ledbg)
        self.stg_2_canvas.itemconfig(self.led_yellow_2, fill=self.yellow)
        self.stg_2_canvas.itemconfig(self.led_green_2, fill=self.ledbg)

    def _set_stg2_green(self):
        self.stg_2_canvas.itemconfig(self.led_red_2, fill=self.ledbg)
        self.stg_2_canvas.itemconfig(self.led_yellow_2, fill=self.ledbg)
        self.stg_2_canvas.itemconfig(self.led_green_2, fill=self.green)


##################################################################################################
# MAIN
parser = argparse.ArgumentParser()
parser.add_argument("-t", help="provides a manual grab button for image sequences",
                    action="store_true")
parser.add_argument("-vc", help="streams imagery from a virtual camera instead of the hardware\
                                 camera", action="store_true")
parser.add_argument("-vs", help="using a virtual stage-controller with limited function instead\
                                 of the actual hardware", action="store_true")
parser.add_argument("-find_center", help="upon startup, find the center positions of the 2 stages",
                                   action="store_true")
args = parser.parse_args()
MANUAL_GRAB_OPTION = False
VIRTUAL_CAMERA = False
VIRTUAL_STAGE_CONTROLLER = False
FIND_CENTER = False
if args.t:
    print("including manual sequence grab functionality")
    MANUAL_GRAB_OPTION = True
if args.vc:
    print("using Virtual Camera")
    VIRTUAL_CAMERA = True
if args.vs:
    print("using Virtual Stage Controller")
    VIRTUAL_STAGE_CONTROLLER = True
if args.find_center:
    print("find stage-2 center ...")
    FIND_CENTER = True

root = tk.Tk()
#root.wm_attributes('-topmost', 'true')
MplMap.settings(root)
Tk_Handler(root, MplMap.get_canvas(), MplMap.get_canvas2(), PlotLine())

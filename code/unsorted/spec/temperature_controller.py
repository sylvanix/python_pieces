'''

'''

from socket import *
import time
import queue
from threading import Thread

from lakeshore import Model336

##################################################################################################
def send_udp_message(msg):
    host = "127.0.0.1" # ip-address of the target (localhost in this case)
    port = 20001
    addr = (host, port)
    UDPSock = socket(AF_INET, SOCK_DGRAM)
    UDPSock.sendto(msg.encode('utf-8'), addr)
    UDPSock.close()

##################################################################################################
def interpret_event_register_bits(opstStr):
    #see page 100 of the model 336 manual (section 6.2.5.2)
    outlist = []
    b7,b6,b5,b4,b3,b2,b1,b0 = [x for x in opstStr]
    if int(b7): outlist.append(" *** Processor Communications Error (COM)")
    if int(b6): outlist.append(" *** Calibration Error (CAL)")
    if int(b5): outlist.append(" *** Autotune Error (ATUNE)")
    if int(b4): outlist.append(" *** New Sensor Reading (NRDG)")
    if int(b3): outlist.append(" *** Loop 1 Ramp Done (RAMP1)")
    if int(b2): outlist.append(" *** Loop 2 Ramp Done (RAMP2)")
    if int(b1): outlist.append(" *** Sensor Overload (OVLD)")
    if int(b0): outlist.append(" *** Alarming (ALARM)")
    return outlist

##################################################################################################
def hexStr2binStr(hexStr):
    return( bin(int(hexStr, 16))[2:].zfill(8) )
    
def print_opst(opst_dict):
    chans = sorted([x for x in opst_dict])
    for chan in chans:
        print(chan, ': ', opst_dict[chan] ,sep='')
    print('')
    
def print_temperatures(temps_dict):
    chans = sorted([x for x in temps_dict])
    for chan in chans:
        print(chan, ': ', temps_dict[chan] ,sep='')
    print('')
##################################################################################################
class LSModel336(Thread):

    def __init__(self, temp_check_interval, thresholds, ip_addresses,
                 to_tempController_queue, from_tempController_queue, from_errors_queue):
        super().__init__()
        self.to_tempController_queue = to_tempController_queue
        self.from_tempController_queue = from_tempController_queue
        self.from_errors_queue = from_errors_queue
        self.thresholds = thresholds
        self.ip_addresses = ip_addresses

        # instantiate the instruments
        self.ip_address_1 = ip_addresses[0]
        self.ip_address_2 = ip_addresses[1]
        self.ip_address_3 = ip_addresses[2]
        self.ip_address_4 = ip_addresses[3]

        # define instruments and channels used
        self.channels = [1,1,1,1, 0,0,0,0, 0,0,0,0, 0,0,0,0]
        isInstrument = [ any(self.channels[0:3]), any(self.channels[4:7]),
                         any(self.channels[8:11]), any(self.channels[12:15]) ]
        label_names = ['A','B','C','D']
        channel_labels = [ label_names[ii%len(label_names)]
                           if self.channels[ii]!=0
                           else ' '
                           for ii in range(len(self.channels)) ]
        self.instruments = {}
        iinst = 0
        for ii in range(len(channel_labels)):
            ilabel = ii%4
            if ilabel == 0:
                iinst += 1
                if isInstrument[iinst-1]:
                    self.instruments[iinst] = []
            if self.channels[ii] != 0:
                self.instruments[iinst].append(label_names[ilabel])

        # instantiate the defined instruments
        if 1 in self.instruments:
            self.I_1 = Model336(ip_address=self.ip_address_1)
        if 2 in self.instruments:
            self.I_2 = Model336(ip_address=self.ip_address_2)
        if 3 in self.instruments:
            self.I_3 = Model336(ip_address=self.ip_address_2)
        if 4 in self.instruments:
            self.I_4 = Model336(ip_address=self.ip_address_4)

        self.keep_running = True
        self.check_temperatures_interval = temp_check_interval
        self.check_opstat_interval = 11.3
        self.elapsed_time = 0.0
        self.start_time = time.time()
        self.temperature_elapsed_time = 0.0
        self.temperature_start_time = time.time()
        self.opstat_elapsed_time = 0.0
        self.opstat_start_time = time.time()

        # initialize temps_dict
        self.temps_dict = {}
        for x in self.instruments:
            self.temps_dict[x] = {}
            for y in self.instruments[x]:
                self.temps_dict[x][y] = [] 

        # initialize errors_dict
        self.errors_dict = {}
        for x in self.instruments:
            self.errors_dict[x] = {}
            for y in self.instruments[x]:
                self.errors_dict[x][y] = []

        # thresholds_dict
        self.thresholds_dict = {}
        for x in self.instruments:
            self.thresholds_dict[x] = {}
            for y in self.instruments[x]:
                self.thresholds_dict[x][y] = {}
                if x == 1: # Instrument 1
                    if y == 'A':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[0][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[0][1]
                    if y == 'B':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[1][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[1][1]
                    if y == 'C':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[2][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[2][1]
                    if y == 'D':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[3][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[3][1]
                if x == 2: # Instrument 2
                    if y == 'A':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[4][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[4][1]
                    if y == 'B':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[5][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[5][1]
                    if y == 'C':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[6][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[6][1]
                    if y == 'D':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[7][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[7][1]
                if x == 3: # Instrument 3
                    if y == 'A':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[8][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[8][1]
                    if y == 'B':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[9][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[9][1]
                    if y == 'C':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[10][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[10][1]
                    if y == 'D':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[11][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[11][1]
                if x == 4: # Instrument 4
                    if y == 'A':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[12][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[12][1]
                    if y == 'B':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[13][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[13][1]
                    if y == 'C':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[14][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[14][1]
                    if y == 'D':
                        self.thresholds_dict[x][y]["upper"] = self.thresholds[15][0]
                        self.thresholds_dict[x][y]["lower"] = self.thresholds[15][1]


    def run(self):
        # This runs when the calling routine invokes the .start method
        while self.keep_running:
            if self.to_tempController_queue.qsize():
                msg = self.to_tempController_queue.get()
                print(msg)
                if ("TEMPERATURE_INTERVAL " in msg):
                    val = float( msg.split(" ")[-1] )
                    self.check_temperatures_interval = val
                if ("ERRORS_INTERVAL " in msg):
                    val = float( msg.split(" ")[-1] )
                    self.check_opstat_interval = val
            else:
                if self._is_time_for_temperature_check():
                    pass
                    self.get_current_temperatures()
                    self.put_to_temps_queue()
                    self.handle_messages()
                if self._is_time_for_opstat_check():
                    self.operational_status_query()
                    self.put_to_errors_queue()
             
            time.sleep(0.1)
            self.elapsed_time = time.time() - self.start_time
            self.temperature_elapsed_time = time.time() - self.temperature_start_time
            self.opstat_elapsed_time = time.time() - self.opstat_start_time

    def stop(self):
        self.keep_running = False

    def _is_time_for_temperature_check(self):
        if self.temperature_elapsed_time > self.check_temperatures_interval:
            self.temperature_start_time = time.time()
            self.temperature_elapsed_time = 0.0
            return 1
        else:
            return 0

    def _is_time_for_opstat_check(self):
        if self.opstat_elapsed_time > self.check_opstat_interval:
            self.opstat_start_time = time.time()
            self.opstat_elapsed_time = 0.0
            return 1
        else:
            return 0

    '''
    Put the current time and temperatues in queue for plotting in the calling function
    '''
    def put_to_temps_queue(self):
        #print(self.temps_dict)
        t = self.elapsed_time
        temps_list = []
        for x in self.temps_dict:
            for y in self.temps_dict[x]:
                temps_list.append(self.temps_dict[x][y])
        temps_list = [f'{t:.3f}'] + temps_list
        self.from_tempController_queue.put(temps_list)

    '''
    Put errors and warnings in the errors_queue
    '''
    def put_to_errors_queue(self):
        t = self.elapsed_time
        errors_list = []
        for x in self.errors_dict:
            for y in self.errors_dict[x]:
                errors_list.append(self.errors_dict[x][y])
        errors_list = [f'{t:.3f}'] + errors_list
        self.from_errors_queue.put(errors_list)

    '''
    Make messages depending on the temperatures of each used channel
    '''
    def handle_messages(self):
        messages = []
        warnings = []
        for x in self.temps_dict:
            chans = sorted([y for y in self.temps_dict[x]])
            for y in self.temps_dict[x]:
                temp = float(self.temps_dict[x][y])
                messages.append( "Temperature " + str(x) + y + ": " + f'{temp:.3f}' )
                if y != chans[-1]: messages += ","

                if temp < self.thresholds_dict[x][y]['lower']:
                    warnings.append( "WARNING " + str(x) + y + ": under temperature threshold." )
                if temp > self.thresholds_dict[x][y]['upper']:
                    warnings.append( "WARNING " + str(x) + y + ": under temperature threshold." )
                if len(warnings)>0:
                    warnStr = " ".join(warnings)
                    send_udp_message(warnStr)

        msgStr = " ".join(messages)
        send_udp_message(msgStr)

    '''
    Reads the current temperature, in degrees Kelvin, for each channel.
    '''
    def get_current_temperatures(self):
        for x in self.instruments:
            if x == 1: instrument = self.I_1
            if x == 2: instrument = self.I_2
            if x == 3: instrument = self.I_3
            if x == 4: instrument = self.I_4
            for y in self.instruments[x]:
                if y == 'A':
                    self.temps_dict[x][y] = instrument.query('CRDG? 1')
                if y == 'B':
                    self.temps_dict[x][y] = instrument.query('CRDG? 2')
                if y == 'C':
                    self.temps_dict[x][y] = instrument.query('CRDG? 3')
                if y == 'D':
                    self.temps_dict[x][y] = instrument.query('CRDG? 4')

    '''
    Send the Operational Status Registry Query
    This register is cleared when it is read.
    '''
    def operational_status_query(self):
        for x in self.instruments:
            if x == 1: instrument = self.I_1
            if x == 2: instrument = self.I_2
            if x == 3: instrument = self.I_3
            if x == 4: instrument = self.I_4
            for y in self.instruments[x]:
                if y == 'A':
                    bits = hexStr2binStr( instrument.query('OPST? 1') )
                    self.errors_dict[x][y] = interpret_event_register_bits(bits)
                if y == 'B':
                    bits = hexStr2binStr( instrument.query('OPST? 2') )
                    self.errors_dict[x][y] = interpret_event_register_bits(bits)
                if y == 'C':
                    bits = hexStr2binStr( instrument.query('OPST? 3') )
                    self.errors_dict[x][y] = interpret_event_register_bits(bits)
                if y == 'D':
                    bits = hexStr2binStr( instrument.query('OPST? 4') )
                    self.errors_dict[x][y] = interpret_event_register_bits(bits)
        #print_opst(self.errors_dict)

    def interpret_event_register_bits(self, opstStr): 
        #see page 100 of the model 336 manual (section 6.2.5.2)
        outlist = []
        b7,b6,b5,b4,b3,b2,b1,b0 = [x for x in opstStr]
        if int(b7): outlist.append(" *** Processor Communications Error (COM)")
        if int(b6): outlist.append(" *** Calibration Error (CAL)")
        if int(b5): outlist.append(" *** Autotune Error (ATUNE)")
        if int(b4): outlist.append(" *** New Sensor Reading (NRDG)")
        if int(b3): outlist.append(" *** Loop 1 Ramp Done (RAMP1)")
        if int(b2): outlist.append(" *** Loop 2 Ramp Done (RAMP2)")
        if int(b1): outlist.append(" *** Sensor Overload (OVLD)")
        if int(b0): outlist.append(" *** Alarming (ALARM)")
        return outlist
     
##################################################################################################

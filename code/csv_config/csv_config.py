import os
import csv
import random
from pathlib import Path

''' write_exposure_times() '''
def write_exposures(expList):
    # save the current file as prev then write new file
    subdirpath = Path("Config")
    subdirpath.mkdir(parents=True, exist_ok=True)
    filename = os.path.join(subdirpath, 'exposure_times.csv')

    # check if file already exists
    if os.path.exists(filename):
        newname = filename.split(".csv")[0] + "_prev.csv"
        if not os.path.exists(newname):
            os.rename(filename, newname)
        else:
            os.replace(filename, newname)

    header = "exposure times " + u'\u03bc'+'Sec.' + '\n'
    with open(filename, "a") as ap:
        if (os.path.getsize(filename)) <= 0:
            ap.write(header)
            for i in range(len(expList)):
                thisVal = str(expList[i])
                ap.write( thisVal + '\n' )
            ap.close()


def read_config_file_csv():
    subdirpath = Path("Config")
    exps = []
    # read the exposures file (if exists)
    fn = os.path.join(subdirpath, 'exposure_times.csv')
    with open(fn, "r") as f:
        reader = csv.reader(f, delimiter=",")
        for i, line in enumerate(reader):
            if i>0 and line: # skipping header row
                exps.append(line[0])
    return exps


# write some times to the exposure config file
times = [ random.uniform(0,100) for _ in range(10) ]
write_exposures(times)

# read back the settings from the file
exposures = read_config_file_csv()
print("exposures:", exposures)

'''
Read in a text file.
Create a dictionary of unique words and their corresponding counts.
Sort the words & counts in descending count order.
Print words and counts.
'''

import nltk

# a tokenizer to keep only words (excludes punctuation)
tokenizer = nltk.RegexpTokenizer(r"\w+")

wordDict = {}

# read the input file
with open("input.txt", "r") as f:
    for line in f:
        words = tokenizer.tokenize(line)
        words = [x.lower() for x in words]
        #print(words)
        for word in words:
            if word not in wordDict:
                wordDict[word] = 1
            else:
                wordDict[word] += 1

# sort dict keys by value (list words in descending count order)
sorted_tuples = sorted(wordDict.items(), key=lambda x: x[1], reverse=True)

for word, count in sorted_tuples:
    print(word, ":", count) 

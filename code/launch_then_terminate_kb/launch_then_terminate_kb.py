'''

'''

import os
import psutil

the_python = '/home/telemaque/.ml36/bin/python3.6'
the_script = 'get_live_frames.py'

# lauch the Python script
os.system(the_python + ' ' + the_script + ' -W ignore &')

# When the user presses spacebar or enters "q", terminate
# the script (while loop).

while True:
    key = input()
    if key in ['q', '']:
        print('Terminating...')

        # find and terminate the script process
        for process in psutil.process_iter():
            if process.cmdline() == [the_python, the_script, '-W', 'ignore']:
                print('Process found. Terminating it.')
                process.terminate()    
        break
    else:
        print('Enter "q" to quit the program.')

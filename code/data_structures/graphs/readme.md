## Graphs

**1. Depth First Search (DFS)**  
[Graph Class with a recursive DFS method](./dfs/dfs_example.py) - basic implementation  
[Solving the "Max Island Area" problem with DFS](./dfs/max_area_island.py) - using a slightly modified version of the DFS code above to work with nodes which are part of a 2d array. (Because sometimes we aren't allowed to use Numpy/SciPy okay?)  
[A minimal "coding challenge" DFS solution](./)  

**2. Depth First Search (BFS)**  
[Graph Class with a recursive BFS method](./bfs/bfs_example.py) - basic implementation  
[Solving the "Max Island Area" problem with BFS](./bfs/max_area_island.py) - using a slightly modified version of the BFS code above to work with nodes which are part of a 2d array. (Because sometimes we aren't allowed to use Numpy/SciPy okay?)  
[A minimal "coding challenge" BFS solution](./)  

**3. Comparing the two algorithms**  

